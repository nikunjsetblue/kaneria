import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {HttpService} from '../service/http.service';
import {DomSanitizer} from '@angular/platform-browser';
import {SeoService} from '../service/seo.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  submitted = false;
  AddForm : FormGroup;
  products = [];
  map = ''
  office_address = ''
  constructor(public _seoService: SeoService, public toastr: ToastrService, private formBuilder: FormBuilder, public router: Router, public http: HttpService,public sanitizer : DomSanitizer) { }

  ngOnInit(): void {
    this.seo_data()
    this.map_data()
    this.address_data()
    this.product_data()
    this.AddForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      mobile: ['', [Validators.required]],
      product_id: ['', [Validators.required]],
      message: ['', [Validators.required]],
      recaptcha: ['', [Validators.required]],
    });
  }

  seo_data(){
    var data = {seo_settings_id : 1};
    this.http.PostAPI('users/GetRecordSeoSettings', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this._seoService.updateTitle(resdata.data.contacts_meta_title);
        this._seoService.updateKeyword(resdata.data.contacts_meta_keyword);
        this._seoService.updateDescription(resdata.data.contacts_meta_description);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  alphaOnly(event) {
    var key = event.keyCode;
    return ((key == 32 || key >= 65 && key <= 90) || key == 8);
  }

  address_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordAddress', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.office_address = resdata.data.office_address;
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  map_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordMap', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.map = resdata.data.map_url;
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  mapURL() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.map);
  }

  product_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordProduct', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.products = resdata.data
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  get fval() {
    return this.AddForm.controls;
  }

  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    let data = this.AddForm.value;
    this.http.PostAPI('users/contacts', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
        this.submitted = false;
        this.AddForm.reset();
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
