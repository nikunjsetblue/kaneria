import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAfterFooterComponent } from './admin-after-footer.component';

describe('AdminAfterFooterComponent', () => {
  let component: AdminAfterFooterComponent;
  let fixture: ComponentFixture<AdminAfterFooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAfterFooterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAfterFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
