import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCountersComponent } from './admin-counters.component';

describe('AdminCountersComponent', () => {
  let component: AdminCountersComponent;
  let fixture: ComponentFixture<AdminCountersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminCountersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCountersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
