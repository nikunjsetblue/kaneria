import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DOCUMENT, Location} from '@angular/common';
import {ToastrService} from 'ngx-toastr';
import {AdminHttpService} from '../../service/admin-http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import {SeoService} from '../../service/seo.service';

@Component({
  selector: 'app-admin-media',
  templateUrl: './admin-media.component.html',
  styleUrls: ['./admin-media.component.css']
})
export class AdminMediaComponent implements OnInit {

  submitted = false;
  AddForm : FormGroup;
  media_id = ''
  constructor(public _seoService: SeoService,
              @Inject(DOCUMENT) private document: Document,
              public toastr: ToastrService,
              private formBuilder: FormBuilder,
              public adminhttp: AdminHttpService,
              public router: Router,
              private location: Location,
              private route: ActivatedRoute) {}

  ngOnInit(): void {
    this._seoService.updateTitle('ADMIN - MEDIA');
    this._seoService.updateKeyword('ADMIN - MEDIA');
    this._seoService.updateDescription('ADMIN - MEDIA');
    const media_id = this.route.snapshot.paramMap.get('media_id');
    this.media_id = media_id;
    this.AddForm = this.formBuilder.group({
      media_name: ['', [Validators.required]],
      media_href: ['', [Validators.required]],
    });
    if(this.media_id){
      this.get_data()
    }
  }

  get_data(){
    var data = {media_id:this.media_id};
    this.adminhttp.PostAPI('admin/GetRecordMedia', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.AddForm = this.formBuilder.group({
          media_name: [resdata.data.media_name, [Validators.required]],
          media_href: [resdata.data.media_href, [Validators.required]],
        });
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  get fval() {
    return this.AddForm.controls;
  }

  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    var data = this.AddForm.value;
    var form_data  = new FormData()
    for ( var key in data ) {
      form_data.append(key, data[key]);
    }
    if(this.media_id != '' && this.media_id != null && this.media_id != undefined){
      form_data.append('media_id',this.media_id)
    }
    this.adminhttp.PostAPI('admin/addMedia', form_data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
        this.router.navigate(['admin/media']);
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
