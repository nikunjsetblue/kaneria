import {Component, OnInit, Inject, Renderer2, OnDestroy} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {AdminHttpService} from '../../service/admin-http.service';
import { DOCUMENT } from '@angular/common';
import {SeoService} from '../../service/seo.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  submitted = false;
  AddForm : FormGroup;
  constructor(public _seoService: SeoService, public toastr: ToastrService, private formBuilder: FormBuilder, public router: Router, public adminhttp: AdminHttpService,  @Inject(DOCUMENT) private document: Document) { }

  ngOnInit(): void {
    this._seoService.updateTitle('ADMIN - LOGIN');
    this._seoService.updateKeyword('ADMIN - LOGIN');
    this._seoService.updateDescription('ADMIN - LOGIN');
    this.document.body.classList.add('bg-default');
    this.AddForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required,Validators.minLength(6)]]
    });
  }

  get fval() {
    return this.AddForm.controls;
  }

  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    var data = this.AddForm.value;
    this.adminhttp.PostAPI('admin/login', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.adminhttp.token = resdata.token;
        localStorage.setItem('Admin', JSON.stringify(resdata.data));
        localStorage.setItem('AdminToken', resdata.token);
        this.adminhttp.IsAdminLogin = true;
        this.toastr.success(resdata.message);
        this.router.navigate(['admin/sliders']);
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
