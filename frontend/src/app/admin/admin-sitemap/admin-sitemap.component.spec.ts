import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSitemapComponent } from './admin-sitemap.component';

describe('AdminSitemapComponent', () => {
  let component: AdminSitemapComponent;
  let fixture: ComponentFixture<AdminSitemapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminSitemapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSitemapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
