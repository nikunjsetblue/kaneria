import {Component, Inject, OnInit} from '@angular/core';
import {SeoService} from '../../service/seo.service';
import {DOCUMENT, Location} from '@angular/common';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder} from '@angular/forms';
import {AdminHttpService} from '../../service/admin-http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-admin-sitemap',
  templateUrl: './admin-sitemap.component.html',
  styleUrls: ['./admin-sitemap.component.css']
})
export class AdminSitemapComponent implements OnInit {

  backend_url = environment.backend_url
  constructor(public _seoService: SeoService,
              @Inject(DOCUMENT) private document: Document,
              public toastr: ToastrService,
              private formBuilder: FormBuilder,
              public adminhttp: AdminHttpService,
              public router: Router,
              private location: Location,
              private route: ActivatedRoute) {}

  ngOnInit(): void {
    this._seoService.updateTitle('ADMIN - SITEMAP');
    this._seoService.updateKeyword('ADMIN - SITEMAP');
    this._seoService.updateDescription('ADMIN - SITEMAP');
  }

  sitemap(){
    var data = {};
    this.adminhttp.PostAPI('admin/GenerateSitemap', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
