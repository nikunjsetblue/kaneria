import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DOCUMENT, Location} from '@angular/common';
import {ToastrService} from 'ngx-toastr';
import {AdminHttpService} from '../../service/admin-http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import {SeoService} from '../../service/seo.service';

@Component({
  selector: 'app-admin-map',
  templateUrl: './admin-map.component.html',
  styleUrls: ['./admin-map.component.css']
})
export class AdminMapComponent implements OnInit {

  submitted = false;
  AddForm : FormGroup;
  map = ''
  map_id = '1';
  constructor(public _seoService: SeoService,
              @Inject(DOCUMENT) private document: Document,
              public toastr: ToastrService,
              private formBuilder: FormBuilder,
              public adminhttp: AdminHttpService,
              public router: Router,
              private location: Location,
              private route: ActivatedRoute,
              public sanitizer : DomSanitizer) {}

  ngOnInit(): void {
    this._seoService.updateTitle('ADMIN - MAP');
    this._seoService.updateKeyword('ADMIN - MAP');
    this._seoService.updateDescription('ADMIN - MAP');
    this.AddForm = this.formBuilder.group({
      map_url: ['', [Validators.required]],
    });
    if(this.map_id){
      this.get_data()
    }
  }

  get_data(){
    var data = {map_id:this.map_id};
    this.adminhttp.PostAPI('admin/GetRecordMap', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.AddForm = this.formBuilder.group({
          map_url: [resdata.data.map_url, [Validators.required]]
        });
        this.map = resdata.data.map_url;
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }


  mapURL() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.map);
  }

  get fval() {
    return this.AddForm.controls;
  }

  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    var data = this.AddForm.value;
    var form_data  = new FormData()
    for ( var key in data ) {
      form_data.append(key, data[key]);
    }
    if(this.map_id != '' && this.map_id != null && this.map_id != undefined){
      form_data.append('map_id',this.map_id)
    }
    this.adminhttp.PostAPI('admin/addMap', form_data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
