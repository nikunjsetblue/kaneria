import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAddCounterComponent } from './admin-add-counter.component';

describe('AdminAddCounterComponent', () => {
  let component: AdminAddCounterComponent;
  let fixture: ComponentFixture<AdminAddCounterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAddCounterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAddCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
