import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {AdminHttpService} from '../../service/admin-http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DOCUMENT, Location} from '@angular/common';
import {environment} from '../../../environments/environment';
import {SeoService} from '../../service/seo.service';

@Component({
  selector: 'app-admin-add-counter',
  templateUrl: './admin-add-counter.component.html',
  styleUrls: ['./admin-add-counter.component.css']
})
export class AdminAddCounterComponent implements OnInit {

  submitted = false;
  AddForm : FormGroup;
  counter_id = '';
  title = 'Add';
  constructor(public _seoService: SeoService,
              @Inject(DOCUMENT) private document: Document,
              public toastr: ToastrService,
              private formBuilder: FormBuilder,
              public adminhttp: AdminHttpService,
              public router: Router,
              private location: Location,
              private route: ActivatedRoute) {}

  ngOnInit(): void {
    this._seoService.updateTitle('ADMIN - ADD COUNTER');
    this._seoService.updateKeyword('ADMIN - ADD COUNTER');
    this._seoService.updateDescription('ADMIN - ADD COUNTER');
    this.document.body.classList.add('bg-default');
    const counter_id = this.route.snapshot.paramMap.get('counter_id');
    this.counter_id = counter_id;
    this.AddForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      total: ['', [Validators.required]],
    });
    if(this.counter_id){
      this._seoService.updateTitle('ADMIN - EDIT COUNTER');
      this._seoService.updateKeyword('ADMIN - EDIT COUNTER');
      this._seoService.updateDescription('ADMIN - EDIT COUNTER');
      this.title = 'Edit'
      this.get_data()
    }
  }

  get_data(){
    var data = {counter_id:this.counter_id};
    this.adminhttp.PostAPI('admin/GetRecordCounter', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.AddForm = this.formBuilder.group({
          name: [resdata.data.name, [Validators.required]],
          total: [resdata.data.total, [Validators.required]],
        });
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  get fval() {
    return this.AddForm.controls;
  }

  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    var data = this.AddForm.value;
    var form_data  = new FormData()
    for ( var key in data ) {
      form_data.append(key, data[key]);
    }
    if(this.counter_id != '' && this.counter_id != null && this.counter_id != undefined){
      form_data.append('counter_id',this.counter_id)
    }
    this.adminhttp.PostAPI('admin/addCounter', form_data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
        this.router.navigate(['admin/counters']);
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
