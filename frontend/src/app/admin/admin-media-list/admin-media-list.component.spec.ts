import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMediaListComponent } from './admin-media-list.component';

describe('AdminMediaListComponent', () => {
  let component: AdminMediaListComponent;
  let fixture: ComponentFixture<AdminMediaListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminMediaListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMediaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
