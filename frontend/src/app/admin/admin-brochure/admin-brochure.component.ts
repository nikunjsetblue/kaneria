import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DOCUMENT, Location} from '@angular/common';
import {ToastrService} from 'ngx-toastr';
import {AdminHttpService} from '../../service/admin-http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import {SeoService} from '../../service/seo.service';

@Component({
  selector: 'app-admin-brochure',
  templateUrl: './admin-brochure.component.html',
  styleUrls: ['./admin-brochure.component.css']
})
export class AdminBrochureComponent implements OnInit {

  submitted = false;
  AddForm : FormGroup;
  pdf: File;
  pdf_file_name = '';
  pdf_file = '';
  brochure_id = '1';
  constructor(public _seoService: SeoService,
              @Inject(DOCUMENT) private document: Document,
              public toastr: ToastrService,
              private formBuilder: FormBuilder,
              public adminhttp: AdminHttpService,
              public router: Router,
              private location: Location,
              private route: ActivatedRoute) {}

  ngOnInit(): void {
    this._seoService.updateTitle('ADMIN - BROCHURE');
    this._seoService.updateKeyword('ADMIN - BROCHURE');
    this._seoService.updateDescription('ADMIN - BROCHURE');
    this.AddForm = this.formBuilder.group({
      pdf: ['', [Validators.required]],
    });
    if(this.brochure_id){
      this.get_data()
    }
  }

  get_data(){
    var data = {brochure_id:this.brochure_id};
    this.adminhttp.PostAPI('admin/GetRecordBrochure', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.AddForm = this.formBuilder.group({
          pdf: ['', [Validators.required]]
        });
        this.pdf_file = (resdata.data.brochure_file != '') ? environment.backend_url +''+ resdata.data.brochure_file : '';
        this.pdf_file_name = resdata.data.brochure_file_name;
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  get fval() {
    return this.AddForm.controls;
  }

  upload_pdf(evt) {
    if (evt.target) {
      this.pdf = evt.target.files[0];
      this.pdf_file_name = evt.target.files[0].name;
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.pdf_file = event.target.result;
      }
      reader.readAsDataURL(evt.target.files[0]);
    }
  }

  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    var data = this.AddForm.value;
    var form_data  = new FormData()
    for ( var key in data ) {
      form_data.append(key, data[key]);
    }
    if(this.pdf){
      form_data.append('pdf', this.pdf)
    }
    if(this.brochure_id != '' && this.brochure_id != null && this.brochure_id != undefined){
      form_data.append('brochure_id',this.brochure_id)
    }
    this.adminhttp.PostAPI('admin/addBrochure', form_data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }


}
