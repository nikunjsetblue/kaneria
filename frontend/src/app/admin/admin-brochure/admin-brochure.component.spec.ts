import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminBrochureComponent } from './admin-brochure.component';

describe('AdminBrochureComponent', () => {
  let component: AdminBrochureComponent;
  let fixture: ComponentFixture<AdminBrochureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminBrochureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminBrochureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
