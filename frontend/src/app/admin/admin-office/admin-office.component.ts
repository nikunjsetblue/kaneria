import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SeoService} from '../../service/seo.service';
import {DOCUMENT, Location} from '@angular/common';
import {ToastrService} from 'ngx-toastr';
import {AdminHttpService} from '../../service/admin-http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-admin-office',
  templateUrl: './admin-office.component.html',
  styleUrls: ['./admin-office.component.css']
})
export class AdminOfficeComponent implements OnInit {

  submitted = false;
  AddForm : FormGroup;
  address_id = '1';
  tinymce: any;
  backend_url = environment.backend_url;
  constructor(public _seoService: SeoService,
              @Inject(DOCUMENT) private document: Document,
              public toastr: ToastrService,
              private formBuilder: FormBuilder,
              public adminhttp: AdminHttpService,
              public router: Router,
              private location: Location,
              private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.tinymce = {
      height: 600,
      plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker imagetools textpattern noneditable help formatpainter permanentpen pageembed charmap mentions quickbars linkchecker advtable',
      toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | code',
      tinydrive_token_provider: this.backend_url + 'admin/jwt',
    }
    this._seoService.updateTitle('ADMIN - OFFICE CONTACT');
    this._seoService.updateKeyword('ADMIN - OFFICE CONTACT');
    this._seoService.updateDescription('ADMIN - OFFICE CONTACT');
    this.AddForm = this.formBuilder.group({
      office_address: ['', [Validators.required]]
    });
    if(this.address_id){
      this.get_data()
    }
  }

  get_data(){
    var data = {address_id:this.address_id};
    this.adminhttp.PostAPI('admin/GetRecordAddress', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.AddForm = this.formBuilder.group({
          office_address: [resdata.data.office_address, [Validators.required]]
        });
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  get fval() {
    return this.AddForm.controls;
  }


  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    var data = this.AddForm.value;
    var form_data  = new FormData()
    for ( var key in data ) {
      form_data.append(key, data[key]);
    }
    if(this.address_id != '' && this.address_id != null && this.address_id != undefined){
      form_data.append('address_id',this.address_id)
    }
    this.adminhttp.PostAPI('admin/addAddress', form_data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
