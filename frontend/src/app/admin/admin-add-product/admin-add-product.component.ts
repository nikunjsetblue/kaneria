import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {ActivatedRoute, Router} from "@angular/router";
import {DOCUMENT, Location} from '@angular/common';
import {AdminHttpService} from '../../service/admin-http.service';
import {environment} from '../../../environments/environment';
import {SeoService} from '../../service/seo.service';

@Component({
  selector: 'app-admin-add-product',
  templateUrl: './admin-add-product.component.html',
  styleUrls: ['./admin-add-product.component.css']
})
export class AdminAddProductComponent implements OnInit {

  submitted = false;
  AddForm : FormGroup;
  ckeConfig: any;
  tinymce: any;
  product_id = '';
  title = 'Add';
  image: File;
  cover_image: File;
  mobile_cover_image: File;
  product_image = '';
  product_cover_image = '';
  product_mobile_cover_image = '';
  backend_url = environment.backend_url;
  constructor(public _seoService: SeoService,
              public toastr: ToastrService,
              private formBuilder: FormBuilder,
              public adminhttp: AdminHttpService,
              public router: Router,
              private location: Location,
              private route: ActivatedRoute,
              @Inject(DOCUMENT) private document: Document) {}

  ngOnInit(): void {
    this._seoService.updateTitle('ADMIN - ADD PRODUCT');
    this._seoService.updateKeyword('ADMIN - ADD PRODUCT');
    this._seoService.updateDescription('ADMIN - ADD PRODUCT');
    this.document.body.classList.add('bg-default');
    this.ckeConfig = {
      allowedContent: true,
      //extraPlugins: 'divarea',
      forcePasteAsPlainText: true
    };
    this.tinymce = {
      height: 600,
      plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker imagetools textpattern noneditable help formatpainter permanentpen pageembed charmap mentions quickbars linkchecker advtable',
      toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | code',
      tinydrive_token_provider: this.backend_url + 'admin/jwt',
    }
    const product_id = this.route.snapshot.paramMap.get('product_id');
    this.product_id = product_id;
    if(this.product_id) {
      this._seoService.updateTitle('ADMIN - EDIT PRODUCT');
      this._seoService.updateKeyword('ADMIN - EDIT PRODUCT');
      this._seoService.updateDescription('ADMIN - EDIT PRODUCT');
      this.title = 'Edit'
      this.get_product_data()
    }
    this.AddForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      position: ['', [Validators.required]],
      image: ['', [Validators.required]],
      cover_image: ['', [Validators.required]],
      mobile_cover_image: ['', [Validators.required]],
      description: ['', [Validators.required]],
      benefits: ['', [Validators.required]],
      specifications: ['', [Validators.required]],
      warning: ['', [Validators.required]],
      meta_title: [''],
      meta_keyword: [''],
      meta_description: ['']
    });
  }

  get_product_data(){
    var data = {product_id:this.product_id};
    this.adminhttp.PostAPI('admin/GetRecordProduct', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.AddForm = this.formBuilder.group({
          title: [resdata.data.title, [Validators.required]],
          position: [resdata.data.position, [Validators.required]],
          description: [resdata.data.description, [Validators.required]],
          benefits: [resdata.data.benefits, [Validators.required]],
          specifications: [resdata.data.specifications, [Validators.required]],
          warning: [resdata.data.warning, [Validators.required]],
          meta_title: [resdata.data.meta_title],
          meta_keyword: [resdata.data.meta_keyword],
          meta_description: [resdata.data.meta_description],
          image: [''],
          cover_image: [''],
          mobile_cover_image: ['']
        });
        this.product_image = environment.backend_url +''+ resdata.data.image;
        this.product_cover_image = environment.backend_url +''+ resdata.data.cover_image;
        this.product_mobile_cover_image = environment.backend_url +''+ resdata.data.mobile_cover_image;
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  upload_photo(evt) {
    if (evt.target) {
      this.image = evt.target.files[0];
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.product_image = event.target.result;
      }
      reader.readAsDataURL(evt.target.files[0]);
    }
  }

  upload_cover_photo(evt) {
    if (evt.target) {
      this.cover_image = evt.target.files[0];
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.product_cover_image = event.target.result;
      }
      reader.readAsDataURL(evt.target.files[0]);
    }
  }

  upload_mobile_cover_photo(evt) {
    if (evt.target) {
      this.mobile_cover_image = evt.target.files[0];
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.product_mobile_cover_image = event.target.result;
      }
      reader.readAsDataURL(evt.target.files[0]);
    }
  }

  get fval() {
    return this.AddForm.controls;
  }

  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    var data = this.AddForm.value;
    var form_data  = new FormData()
    for ( let key in data ) {
      form_data.append(key, data[key]);
    }
    if(this.image){
      form_data.append('image',this.image)
    }
    if(this.cover_image){
      form_data.append('cover_image',this.cover_image)
    }
    if(this.mobile_cover_image){
      form_data.append('mobile_cover_image',this.mobile_cover_image)
    }
    if(this.product_id != '' && this.product_id != null && this.product_id != undefined){
      form_data.append('product_id',this.product_id)
    }

    this.adminhttp.PostAPI('admin/addProduct', form_data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
        this.router.navigate(['admin/products']);
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }


}
