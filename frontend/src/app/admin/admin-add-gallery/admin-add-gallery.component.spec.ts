import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAddGalleryComponent } from './admin-add-gallery.component';

describe('AdminAddGalleryComponent', () => {
  let component: AdminAddGalleryComponent;
  let fixture: ComponentFixture<AdminAddGalleryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAddGalleryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAddGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
