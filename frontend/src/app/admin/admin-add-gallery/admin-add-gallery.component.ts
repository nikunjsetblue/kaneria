import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DOCUMENT, Location} from '@angular/common';
import {ToastrService} from 'ngx-toastr';
import {AdminHttpService} from '../../service/admin-http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import {SeoService} from '../../service/seo.service';

@Component({
  selector: 'app-admin-add-gallery',
  templateUrl: './admin-add-gallery.component.html',
  styleUrls: ['./admin-add-gallery.component.css']
})
export class AdminAddGalleryComponent implements OnInit {

  submitted = false;
  AddForm : FormGroup;
  gallery_id = '';
  title = 'Add';
  image: File;
  img = '';
  constructor(public _seoService: SeoService,
              @Inject(DOCUMENT) private document: Document,
              public toastr: ToastrService,
              private formBuilder: FormBuilder,
              public adminhttp: AdminHttpService,
              public router: Router,
              private location: Location,
              private route: ActivatedRoute) {}

  ngOnInit(): void {
    this._seoService.updateTitle('ADMIN - ADD GALLERY');
    this._seoService.updateKeyword('ADMIN - ADD GALLERY');
    this._seoService.updateDescription('ADMIN - ADD GALLERY');
    this.document.body.classList.add('bg-default');
    const gallery_id = this.route.snapshot.paramMap.get('gallery_id');
    this.gallery_id = gallery_id;
    this.AddForm = this.formBuilder.group({
      image: ['', [Validators.required]],
      position: ['', [Validators.required]],
    });
    if(this.gallery_id){
      this._seoService.updateTitle('ADMIN - EDIT GALLERY');
      this._seoService.updateKeyword('ADMIN - EDIT GALLERY');
      this._seoService.updateDescription('ADMIN - EDIT GALLERY');
      this.title = 'Edit'
      this.get_data()
    }
  }

  get_data(){
    var data = {gallery_id:this.gallery_id};
    this.adminhttp.PostAPI('admin/GetRecordGallery', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.AddForm = this.formBuilder.group({
          image: [''],
          position: [resdata.data.position, [Validators.required]],
        });
        this.img = environment.backend_url +''+ resdata.data.image;
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  upload_photo(evt) {
    if (evt.target) {
      this.image = evt.target.files[0];
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.img = event.target.result;
      }
      reader.readAsDataURL(evt.target.files[0]);
    }
  }

  get fval() {
    return this.AddForm.controls;
  }

  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    var data = this.AddForm.value;
    var form_data  = new FormData()
    for ( var key in data ) {
      form_data.append(key, data[key]);
    }
    if(this.image){
      form_data.append('gallery',this.image)
    }
    if(this.gallery_id != '' && this.gallery_id != null && this.gallery_id != undefined){
      form_data.append('gallery_id',this.gallery_id)
    }
    this.adminhttp.PostAPI('admin/addGallery', form_data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
        this.router.navigate(['admin/gallery']);
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
