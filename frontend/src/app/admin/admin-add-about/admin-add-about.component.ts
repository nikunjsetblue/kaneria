import {Component, Inject, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {AdminHttpService} from '../../service/admin-http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DOCUMENT, Location} from '@angular/common';
import {environment} from '../../../environments/environment';
import {SeoService} from '../../service/seo.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic/build/ckeditor';

@Component({
  selector: 'app-admin-add-about',
  templateUrl: './admin-add-about.component.html',
  styleUrls: ['./admin-add-about.component.css']
})
export class AdminAddAboutComponent implements OnInit {

  submitted = false;
  AddForm : FormGroup;
  ckeConfig: any;
  tinymce: any;
  about_id = '';
  title = 'Add';
  image: File;
  about_image = '';
  public Editor = ClassicEditor;
  backend_url = environment.backend_url;
  constructor(@Inject(DOCUMENT) private document: Document,
              public toastr: ToastrService,
              private formBuilder: FormBuilder,
              public adminhttp: AdminHttpService,
              public router: Router,
              private location: Location,
              private route: ActivatedRoute,
              public _seoService: SeoService) {
  }

  ngOnInit(): void {
    this._seoService.updateTitle('ADMIN - ADD ABOUT US');
    this._seoService.updateKeyword('ADMIN - ADD ABOUT US');
    this._seoService.updateDescription('ADMIN - ADD ABOUT US');
    this.document.body.classList.add('bg-default');
    // this.ckeConfig = {
    //   allowedContent: true,
    //   forcePasteAsPlainText: true,
    //   extraPlugins: 'uploadimage',
    //   filebrowserImageUploadUrl: 'http://localhost:5000/upload/editor/',
    // };
    this.tinymce = {
      height: 600,
      plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker imagetools textpattern noneditable help formatpainter permanentpen pageembed charmap mentions quickbars linkchecker advtable',
      toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | code',
      tinydrive_token_provider: this.backend_url + 'admin/jwt',
    }
    const about_id = this.route.snapshot.paramMap.get('about_id');
    this.about_id = about_id;
    this.AddForm = this.formBuilder.group({
      about_title: ['', [Validators.required]],
      position: ['', [Validators.required]],
      about_content2: [''],
      about_content: [''],
      meta_title: [''],
      meta_keyword: [''],
      meta_description: ['']
    });
    if(this.about_id){
      this._seoService.updateTitle('ADMIN - EDIT ABOUT US');
      this._seoService.updateKeyword('ADMIN - EDIT ABOUT US');
      this._seoService.updateDescription('ADMIN - EDIT ABOUT US');
      this.title = 'Edit'
      this.get_data()
    }
  }

  get_data(){
    var data = {about_id:this.about_id};
    this.adminhttp.PostAPI('admin/GetRecordAbout', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.AddForm = this.formBuilder.group({
          about_title: [resdata.data.about_title, [Validators.required]],
          position: [resdata.data.position, [Validators.required]],
          about_content2: [resdata.data.about_content2, [Validators.required]],
          about_content: [resdata.data.about_content, [Validators.required]],
          meta_title: [resdata.data.meta_title],
          meta_keyword: [resdata.data.meta_keyword],
          meta_description: [resdata.data.meta_description],
        });
        this.about_image = (resdata.data.image != '') ? environment.backend_url +''+ resdata.data.image : '';
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  get fval() {
    return this.AddForm.controls;
  }

  upload_photo(evt) {
    if (evt.target) {
      this.image = evt.target.files[0];
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.about_image = event.target.result;
      }
      reader.readAsDataURL(evt.target.files[0]);
    }
  }

  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    var data = this.AddForm.value;
    var form_data  = new FormData()
    for ( var key in data ) {
      form_data.append(key, data[key]);
    }
    if(this.image){
      form_data.append('image', this.image)
    }
    if(this.about_id != '' && this.about_id != null && this.about_id != undefined){
      form_data.append('about_id',this.about_id)
    }
    this.adminhttp.PostAPI('admin/addAbout', form_data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
        this.router.navigate(['admin/about']);
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
