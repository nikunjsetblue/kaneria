import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAddAboutComponent } from './admin-add-about.component';

describe('AdminAddAboutComponent', () => {
  let component: AdminAddAboutComponent;
  let fixture: ComponentFixture<AdminAddAboutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAddAboutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAddAboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
