import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DOCUMENT, Location} from '@angular/common';
import {ToastrService} from 'ngx-toastr';
import {AdminHttpService} from '../../service/admin-http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SeoService} from '../../service/seo.service';

@Component({
  selector: 'app-admin-add-vacancy',
  templateUrl: './admin-add-vacancy.component.html',
  styleUrls: ['./admin-add-vacancy.component.css']
})
export class AdminAddVacancyComponent implements OnInit {

  submitted = false;
  AddForm : FormGroup;
  vacancy_id = '';
  title = 'Add';
  constructor(public _seoService: SeoService,
              @Inject(DOCUMENT) private document: Document,
              public toastr: ToastrService,
              private formBuilder: FormBuilder,
              public adminhttp: AdminHttpService,
              public router: Router,
              private location: Location,
              private route: ActivatedRoute) {}

  ngOnInit(): void {
    this._seoService.updateTitle('ADMIN - ADD VACANCY');
    this._seoService.updateKeyword('ADMIN - ADD VACANCY');
    this._seoService.updateDescription('ADMIN - ADD VACANCY');
    this.document.body.classList.add('bg-default');
    const vacancy_id = this.route.snapshot.paramMap.get('vacancy_id');
    this.vacancy_id = vacancy_id;
    this.AddForm = this.formBuilder.group({
      vacancy_title: ['', [Validators.required]],
      vacancy_exp: [''],
    });
    if(this.vacancy_id){
      this._seoService.updateTitle('ADMIN - EDIT VACANCY');
      this._seoService.updateKeyword('ADMIN - EDIT VACANCY');
      this._seoService.updateDescription('ADMIN - EDIT VACANCY');
      this.title = 'Edit'
      this.get_data()
    }
  }

  get_data(){
    var data = {vacancy_id:this.vacancy_id};
    this.adminhttp.PostAPI('admin/GetRecordVacancy', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.AddForm = this.formBuilder.group({
          vacancy_title: [resdata.data.vacancy_title, [Validators.required]],
          vacancy_exp: [resdata.data.vacancy_exp],
        });
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  get fval() {
    return this.AddForm.controls;
  }

  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    var data = this.AddForm.value;
    var form_data  = new FormData()
    for ( var key in data ) {
      form_data.append(key, data[key]);
    }
    if(this.vacancy_id != '' && this.vacancy_id != null && this.vacancy_id != undefined){
      form_data.append('vacancy_id',this.vacancy_id)
    }
    this.adminhttp.PostAPI('admin/addVacancy', form_data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
        this.router.navigate(['admin/vacancy']);
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
