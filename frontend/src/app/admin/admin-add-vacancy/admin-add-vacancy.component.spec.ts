import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAddVacancyComponent } from './admin-add-vacancy.component';

describe('AdminAddVacancyComponent', () => {
  let component: AdminAddVacancyComponent;
  let fixture: ComponentFixture<AdminAddVacancyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAddVacancyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAddVacancyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
