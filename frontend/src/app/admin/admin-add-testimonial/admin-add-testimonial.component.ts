import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {AdminHttpService} from '../../service/admin-http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DOCUMENT, Location} from '@angular/common';
import {environment} from '../../../environments/environment';
import {SeoService} from '../../service/seo.service';

@Component({
  selector: 'app-admin-add-testimonial',
  templateUrl: './admin-add-testimonial.component.html',
  styleUrls: ['./admin-add-testimonial.component.css']
})
export class AdminAddTestimonialComponent implements OnInit {

  submitted = false;
  AddForm : FormGroup;
  testimonial_id = '';
  title = 'Add';
  image: File;
  testi_image = '';
  constructor(public _seoService: SeoService,
              @Inject(DOCUMENT) private document: Document,
              public toastr: ToastrService,
              private formBuilder: FormBuilder,
              public adminhttp: AdminHttpService,
              public router: Router,
              private location: Location,
              private route: ActivatedRoute) {}

  ngOnInit(): void {
    this._seoService.updateTitle('ADMIN - ADD TESTIMONIAL');
    this._seoService.updateKeyword('ADMIN - ADD TESTIMONIAL');
    this._seoService.updateDescription('ADMIN - ADD TESTIMONIAL');
    this.document.body.classList.add('bg-default');
    const testimonial_id = this.route.snapshot.paramMap.get('testimonial_id');
    this.testimonial_id = testimonial_id;
    this.AddForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      position: ['', [Validators.required]],
      image: ['', [Validators.required]],
      description: ['', [Validators.required]],
    });
    if(this.testimonial_id){
      this._seoService.updateTitle('ADMIN - EDIT TESTIMONIAL');
      this._seoService.updateKeyword('ADMIN - EDIT TESTIMONIAL');
      this._seoService.updateDescription('ADMIN - EDIT TESTIMONIAL');
      this.title = 'Edit'
      this.get_data()
    }
  }

  get_data(){
    var data = {testimonial_id:this.testimonial_id};
    this.adminhttp.PostAPI('admin/GetRecordTestimonial', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.AddForm = this.formBuilder.group({
          name: [resdata.data.name, [Validators.required]],
          position: [resdata.data.position, [Validators.required]],
          image: [''],
          description: [resdata.data.description, [Validators.required]],
        });
        this.testi_image = (resdata.data.image != '') ? environment.backend_url +''+ resdata.data.image : '';
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  upload_photo(evt) {
    if (evt.target) {
      this.image = evt.target.files[0];
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.testi_image = event.target.result;
      }
      reader.readAsDataURL(evt.target.files[0]);
    }
  }

  get fval() {
    return this.AddForm.controls;
  }

  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    var data = this.AddForm.value;
    var form_data  = new FormData()
    for ( var key in data ) {
      form_data.append(key, data[key]);
    }
    if(this.image){
      form_data.append('image',this.image)
    }
    if(this.testimonial_id != '' && this.testimonial_id != null && this.testimonial_id != undefined){
      form_data.append('testimonial_id',this.testimonial_id)
    }
    this.adminhttp.PostAPI('admin/addTestimonial', form_data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
        this.router.navigate(['admin/testimonials']);
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
