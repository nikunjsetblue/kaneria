import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAddTestimonialComponent } from './admin-add-testimonial.component';

describe('AdminAddTestimonialComponent', () => {
  let component: AdminAddTestimonialComponent;
  let fixture: ComponentFixture<AdminAddTestimonialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAddTestimonialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAddTestimonialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
