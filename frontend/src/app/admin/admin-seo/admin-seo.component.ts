import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DOCUMENT, Location} from '@angular/common';
import {ToastrService} from 'ngx-toastr';
import {AdminHttpService} from '../../service/admin-http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import {SeoService} from '../../service/seo.service';

@Component({
  selector: 'app-admin-seo',
  templateUrl: './admin-seo.component.html',
  styleUrls: ['./admin-seo.component.css']
})
export class AdminSeoComponent implements OnInit {

  submitted = false;
  AddForm : FormGroup;
  seo_settings_id = '1';
  constructor(public _seoService: SeoService,
              @Inject(DOCUMENT) private document: Document,
              public toastr: ToastrService,
              private formBuilder: FormBuilder,
              public adminhttp: AdminHttpService,
              public router: Router,
              private location: Location,
              private route: ActivatedRoute) {}

  ngOnInit(): void {
    this._seoService.updateTitle('ADMIN - SEO SETTINGS');
    this._seoService.updateKeyword('ADMIN - SEO SETTINGS');
    this._seoService.updateDescription('ADMIN - SEO SETTINGS');
    this.AddForm = this.formBuilder.group({
      google_analytics: [''],
      home_meta_title: ['', [Validators.required]],
      home_meta_keyword: ['', [Validators.required]],
      home_meta_description: ['', [Validators.required]],
      careers_meta_title: ['', [Validators.required]],
      careers_meta_keyword: ['', [Validators.required]],
      careers_meta_description: ['', [Validators.required]],
      contacts_meta_title: ['', [Validators.required]],
      contacts_meta_keyword: ['', [Validators.required]],
      contacts_meta_description: ['', [Validators.required]],
      clients_meta_title: ['', [Validators.required]],
      clients_meta_keyword: ['', [Validators.required]],
      clients_meta_description: ['', [Validators.required]],
      vendor_meta_title: ['', [Validators.required]],
      vendor_meta_keyword: ['', [Validators.required]],
      vendor_meta_description: ['', [Validators.required]],
    });
    if(this.seo_settings_id){
      this.get_data()
    }
  }

  get_data(){
    var data = {seo_settings_id:this.seo_settings_id};
    this.adminhttp.PostAPI('admin/GetRecordSeoSettings', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.AddForm = this.formBuilder.group({
          google_analytics: [resdata.data.google_analytics],
          home_meta_title: [resdata.data.home_meta_title, [Validators.required]],
          home_meta_keyword: [resdata.data.home_meta_keyword, [Validators.required]],
          home_meta_description: [resdata.data.home_meta_description, [Validators.required]],
          careers_meta_title: [resdata.data.careers_meta_title, [Validators.required]],
          careers_meta_keyword: [resdata.data.careers_meta_keyword, [Validators.required]],
          careers_meta_description: [resdata.data.careers_meta_description, [Validators.required]],
          contacts_meta_title: [resdata.data.contacts_meta_title, [Validators.required]],
          contacts_meta_keyword: [resdata.data.contacts_meta_keyword, [Validators.required]],
          contacts_meta_description: [resdata.data.contacts_meta_description, [Validators.required]],
          clients_meta_title: [resdata.data.clients_meta_title, [Validators.required]],
          clients_meta_keyword: [resdata.data.clients_meta_keyword, [Validators.required]],
          clients_meta_description: [resdata.data.clients_meta_description, [Validators.required]],
          vendor_meta_title: [resdata.data.vendor_meta_title, [Validators.required]],
          vendor_meta_keyword: [resdata.data.vendor_meta_keyword, [Validators.required]],
          vendor_meta_description: [resdata.data.vendor_meta_description, [Validators.required]],
        });
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  get fval() {
    return this.AddForm.controls;
  }


  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    var data = this.AddForm.value;
    var form_data  = new FormData()
    for ( var key in data ) {
      form_data.append(key, data[key]);
    }
    if(this.seo_settings_id != '' && this.seo_settings_id != null && this.seo_settings_id != undefined){
      form_data.append('seo_settings_id',this.seo_settings_id)
    }
    this.adminhttp.PostAPI('admin/addSeoSettings', form_data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
