import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSeoComponent } from './admin-seo.component';

describe('AdminSeoComponent', () => {
  let component: AdminSeoComponent;
  let fixture: ComponentFixture<AdminSeoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminSeoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSeoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
