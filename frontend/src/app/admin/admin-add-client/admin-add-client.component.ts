import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {AdminHttpService} from '../../service/admin-http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DOCUMENT, Location} from '@angular/common';
import {environment} from '../../../environments/environment';
import {SeoService} from '../../service/seo.service';

@Component({
  selector: 'app-admin-add-client',
  templateUrl: './admin-add-client.component.html',
  styleUrls: ['./admin-add-client.component.css']
})
export class AdminAddClientComponent implements OnInit {

  submitted = false;
  AddForm : FormGroup;
  client_id = '';
  title = 'Add';
  image: File;
  client_img = '';

  constructor(public _seoService: SeoService,
              @Inject(DOCUMENT) private document: Document,
              public toastr: ToastrService,
              private formBuilder: FormBuilder,
              public adminhttp: AdminHttpService,
              public router: Router,
              private location: Location,
              private route: ActivatedRoute) {}

  ngOnInit(): void {
    this._seoService.updateTitle('ADMIN - ADD CLIENTS');
    this._seoService.updateKeyword('ADMIN - ADD CLIENTS');
    this._seoService.updateDescription('ADMIN - ADD CLIENTS');
    this.document.body.classList.add('bg-default');
    const client_id = this.route.snapshot.paramMap.get('client_id');
    this.client_id = client_id;
    this.AddForm = this.formBuilder.group({
      image: ['', [Validators.required]],
    });
    if(this.client_id){
      this._seoService.updateTitle('ADMIN - EDIT CLIENTS');
      this._seoService.updateKeyword('ADMIN - EDIT CLIENTS');
      this._seoService.updateDescription('ADMIN - EDIT CLIENTS');
      this.title = 'Edit'
      this.get_data()
    }
  }

  get_data(){
    var data = {client_id:this.client_id};
    this.adminhttp.PostAPI('admin/GetRecordClient', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.AddForm = this.formBuilder.group({
          image: ['']
        });
        this.client_img = environment.backend_url +''+ resdata.data.image;
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  upload_photo(evt) {
    if (evt.target) {
      this.image = evt.target.files[0];
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.client_img = event.target.result;
      }
      reader.readAsDataURL(evt.target.files[0]);
    }
  }

  get fval() {
    return this.AddForm.controls;
  }

  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    var data = this.AddForm.value;
    var form_data  = new FormData()
    for ( var key in data ) {
      form_data.append(key, data[key]);
    }
    if(this.image){
      form_data.append('image',this.image)
    }
    if(this.client_id != '' && this.client_id != null && this.client_id != undefined){
      form_data.append('client_id',this.client_id)
    }
    this.adminhttp.PostAPI('admin/addClient', form_data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
        this.router.navigate(['admin/clients']);
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
