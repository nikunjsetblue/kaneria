import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {AdminHttpService} from '../../service/admin-http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DOCUMENT, Location} from '@angular/common';
import {environment} from '../../../environments/environment';
import {SeoService} from '../../service/seo.service';

@Component({
  selector: 'app-admin-add-slider',
  templateUrl: './admin-add-slider.component.html',
  styleUrls: ['./admin-add-slider.component.css']
})
export class AdminAddSliderComponent implements OnInit {

  submitted = false;
  AddForm : FormGroup;
  ckeConfig: any;
  banner_id = '';
  title = 'Add';
  desktop_image: File;
  mobile_image: File;
  desk_img = '';
  mob_img = '';
  constructor(public _seoService: SeoService,
              @Inject(DOCUMENT) private document: Document,
              public toastr: ToastrService,
              private formBuilder: FormBuilder,
              public adminhttp: AdminHttpService,
              public router: Router,
              private location: Location,
              private route: ActivatedRoute) {}

  ngOnInit(): void {
    this._seoService.updateTitle('ADMIN - ADD SLIDER');
    this._seoService.updateKeyword('ADMIN - ADD SLIDER');
    this._seoService.updateDescription('ADMIN - ADD SLIDER');
    this.document.body.classList.add('bg-default');
    this.ckeConfig = {
      allowedContent: false,
      extraPlugins: 'divarea',
      forcePasteAsPlainText: true
    };
    const banner_id = this.route.snapshot.paramMap.get('banner_id');
    this.banner_id = banner_id;
    this.AddForm = this.formBuilder.group({
      desktop_img: ['', [Validators.required]],
      mobile_img: ['', [Validators.required]],
      position: ['', [Validators.required]],
      link: ['', [Validators.required]],
    });
    if(this.banner_id){
      this._seoService.updateTitle('ADMIN - EDIT SLIDER');
      this._seoService.updateKeyword('ADMIN - EDIT SLIDER');
      this._seoService.updateDescription('ADMIN - EDIT SLIDER');
      this.title = 'Edit'
      this.get_data()
    }
  }

  get_data(){
    var data = {banner_id:this.banner_id};
    this.adminhttp.PostAPI('admin/GetRecordSlider', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.AddForm = this.formBuilder.group({
          desktop_img: [''],
          mobile_img: [''],
          link: [resdata.data.link, [Validators.required]],
          position: [resdata.data.position, [Validators.required]],
        });
        this.desk_img = environment.backend_url +''+ resdata.data.desktop_img;
        this.mob_img = environment.backend_url +''+ resdata.data.mobile_img;
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  upload_photo(evt) {
    if (evt.target) {
      this.desktop_image = evt.target.files[0];
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.desk_img = event.target.result;
      }
      reader.readAsDataURL(evt.target.files[0]);
    }
  }

  upload_cover_photo(evt) {
    if (evt.target) {
      this.mobile_image = evt.target.files[0];
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.mob_img = event.target.result;
      }
      reader.readAsDataURL(evt.target.files[0]);
    }
  }

  get fval() {
    return this.AddForm.controls;
  }

  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    var data = this.AddForm.value;
    var form_data  = new FormData()
    for ( var key in data ) {
      form_data.append(key, data[key]);
    }
    if(this.desktop_image){
      form_data.append('desktop_img',this.desktop_image)
    }
    if(this.mobile_image){
      form_data.append('mobile_img',this.mobile_image)
    }
    if(this.banner_id != '' && this.banner_id != null && this.banner_id != undefined){
      form_data.append('banner_id',this.banner_id)
    }
    this.adminhttp.PostAPI('admin/addSlider', form_data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
        this.router.navigate(['admin/sliders']);
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
