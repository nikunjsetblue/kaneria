import { Component, OnInit } from '@angular/core';
import {OwlOptions} from 'ngx-owl-carousel-o';
import {environment} from '../../environments/environment';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {HttpService} from '../service/http.service';
import {SeoService} from '../service/seo.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  clients = [];
  backend_url = ''

  logoOptions:OwlOptions = {
    loop: true,
    margin: 0,
    nav: false,
    dots: false,
    smartSpeed: 1000,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    responsive: { 0: { items: 2 }, 600: { items: 3 }, 1000: { items: 6 } },
  }

  constructor(public _seoService: SeoService, public toastr: ToastrService, private formBuilder: FormBuilder, public router: Router, public http: HttpService) { }

  ngOnInit(): void {
    this.seo_data();
    this.backend_url = environment.backend_url;
    this.client_data();
  }

  seo_data(){
    var data = {seo_settings_id : 1};
    this.http.PostAPI('users/GetRecordSeoSettings', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this._seoService.updateTitle(resdata.data.clients_meta_title);
        this._seoService.updateKeyword(resdata.data.clients_meta_keyword);
        this._seoService.updateDescription(resdata.data.clients_meta_description);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  client_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordClient', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.clients = resdata.data
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
