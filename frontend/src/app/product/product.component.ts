import { Component, OnInit } from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {HttpService} from '../service/http.service';
import {environment} from '../../environments/environment';
import {filter, map, mergeMap} from 'rxjs/operators';
import {SeoService} from '../service/seo.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  slug = ''
  title = ''
  description = ''
  benefits = ''
  specifications = ''
  warning = ''
  cover_image = ''
  mobile_cover_image = ''
  loadAPI: Promise<any>;
  constructor(public _seoService: SeoService, public toastr: ToastrService, private formBuilder: FormBuilder, public router: Router, public http: HttpService, private route: ActivatedRoute) {
    route.params.subscribe(params => {
      this.slug = params['slug'];
      this.product_data();
    })

  }

  ngOnInit(): void {
  }

  product_data(){
    var data = {slug:this.slug};
    this.http.PostAPI('users/GetRecordProduct', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.title = resdata.data[0].title
        this.description = resdata.data[0].description
        this.benefits = resdata.data[0].benefits
        this.warning = resdata.data[0].warning
        this.specifications = resdata.data[0].specifications
        this.cover_image = (resdata.data[0].cover_image != '') ? environment.backend_url+''+resdata.data[0].cover_image : ''
        this.mobile_cover_image = (resdata.data[0].mobile_cover_image != '') ? environment.backend_url+''+resdata.data[0].mobile_cover_image : '';
        this._seoService.updateTitle(resdata.data[0].meta_title);
        this._seoService.updateKeyword(resdata.data[0].meta_keyword);
        this._seoService.updateDescription(resdata.data[0].meta_description);
        this.loadAPI = new Promise((resolve) => {
          this.loadScript();
          resolve(true);
        });
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  public loadScript() {
    var isFound = false;
    var scripts = document.getElementsByTagName("script")
    for (var i = 0; i < scripts.length; ++i) {
      if (scripts[i].getAttribute('src') != null && scripts[i].getAttribute('src').includes("loader")) {
        isFound = true;
      }
    }

    if (!isFound) {
      var dynamicScripts = ['assets/js/product.js'];

      for (var i = 0; i < dynamicScripts.length; i++) {
        let node = document.createElement('script');
        node.src = dynamicScripts [i];
        node.type = 'text/javascript';
        node.async = false;
        node.charset = 'utf-8';
        document.getElementsByTagName('body')[0].appendChild(node);
      }

    }
  }

}
