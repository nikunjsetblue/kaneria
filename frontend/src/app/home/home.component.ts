import {AfterViewInit, Component, OnInit} from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import {Observable, Observer} from 'rxjs';
import {ToastrService} from "ngx-toastr";
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {HttpService} from '../service/http.service';
import {environment} from '../../environments/environment';
import {SeoService} from '../service/seo.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  banners = [];
  counters = [];
  products = [];
  abouts = [];
  clients = [];
  testimonials = [];
  step:number = 0;
  backend_url = ''

  bannerOptions: OwlOptions = {
    items: 1,
    loop: true,
    margin: 0,
    nav: false,
    dots: true,
    smartSpeed: 1000,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true
  }

  logoOptions:OwlOptions = {
    loop: true,
    margin: 0,
    nav: false,
    dots: false,
    smartSpeed: 1000,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    responsive: { 0: { items: 2 }, 600: { items: 3 }, 1000: { items: 6 } },
  }

  clientOptions:OwlOptions = {
    loop: true,
    margin: 0,
    nav: false,
    dots: true,
    smartSpeed: 2000,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    responsive: { 0: { items: 1 }, 600: { items: 2 }, 1000: { items: 2 } },
  }

  constructor(public _seoService: SeoService, public toastr: ToastrService, private formBuilder: FormBuilder, public router: Router, public http: HttpService) {}

  ngOnInit(): void {
    this.seo_data()
    this.backend_url = environment.backend_url;
    this.banner_data()
    this.product_data()
    this.counter_data()
    this.about_data()
    this.client_data()
    this.testimonial_data()
  }

  seo_data(){
    var data = {seo_settings_id : 1};
    this.http.PostAPI('users/GetRecordSeoSettings', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this._seoService.updateTitle(resdata.data.home_meta_title);
        this._seoService.updateKeyword(resdata.data.home_meta_keyword);
        this._seoService.updateDescription(resdata.data.home_meta_description);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  banner_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordBanner', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.banners = resdata.data
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  product_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordProduct', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.products = resdata.data
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  counter_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordCounter', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.counters = resdata.data
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  about_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordAbout', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.abouts = resdata.data
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  check_step(step){
    this.step = step;
  }

  client_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordClient', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.clients = resdata.data
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  testimonial_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordTestimonials', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.testimonials = resdata.data
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }
}
