import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {AboutComponent} from './about/about.component';
import {ContactComponent} from './contact/contact.component';
import {CareersComponent} from './careers/careers.component';
import {ProductComponent} from './product/product.component';
import {GalleryComponent} from './gallery/gallery.component';
import {AdminLoginComponent} from './admin/admin-login/admin-login.component';
import {AdminGuestGuardService} from './service/admin-guest-guard.service';
import {AdminForgotPasswordComponent} from './admin/admin-forgot-password/admin-forgot-password.component';
import {DashboardComponent} from './admin/dashboard/dashboard.component';
import {AdminAuthGuardService} from './service/admin-auth-guard.service';
import {AdminProductsComponent} from './admin/admin-products/admin-products.component';
import {AdminAddProductComponent} from './admin/admin-add-product/admin-add-product.component';
import {AdminSlidersComponent} from './admin/admin-sliders/admin-sliders.component';
import {AdminAddSliderComponent} from './admin/admin-add-slider/admin-add-slider.component';
import {AdminCountersComponent} from './admin/admin-counters/admin-counters.component';
import {AdminAddCounterComponent} from './admin/admin-add-counter/admin-add-counter.component';
import {AdminAboutComponent} from './admin/admin-about/admin-about.component';
import {AdminAddAboutComponent} from './admin/admin-add-about/admin-add-about.component';
import {AdminClientsComponent} from './admin/admin-clients/admin-clients.component';
import {AdminAddClientComponent} from './admin/admin-add-client/admin-add-client.component';
import {AdminTestimonialsComponent} from './admin/admin-testimonials/admin-testimonials.component';
import {AdminAddTestimonialComponent} from './admin/admin-add-testimonial/admin-add-testimonial.component';
import {AdminCareersComponent} from './admin/admin-careers/admin-careers.component';
import {AdminContactComponent} from './admin/admin-contact/admin-contact.component';
import {AdminBrochureComponent} from './admin/admin-brochure/admin-brochure.component';
import {AdminMediaComponent} from './admin/admin-media/admin-media.component';
import {AdminMediaListComponent} from './admin/admin-media-list/admin-media-list.component';
import {AdminGalleryComponent} from './admin/admin-gallery/admin-gallery.component';
import {AdminAddGalleryComponent} from './admin/admin-add-gallery/admin-add-gallery.component';
import {AdminMapComponent} from './admin/admin-map/admin-map.component';
import {AdminVacancyComponent} from './admin/admin-vacancy/admin-vacancy.component';
import {AdminAddVacancyComponent} from './admin/admin-add-vacancy/admin-add-vacancy.component';
import {ClientComponent} from './client/client.component';
import {AdminSeoComponent} from './admin/admin-seo/admin-seo.component';
import {AdminSitemapComponent} from './admin/admin-sitemap/admin-sitemap.component';
import {VendorComponent} from './vendor/vendor.component';
import {AdminVendorComponent} from './admin/admin-vendor/admin-vendor.component';
import {UploaderComponent} from './uploader/uploader.component';
import {AdminOfficeComponent} from './admin/admin-office/admin-office.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'prefix',
    component: HomeComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'about/:slug',
    component: AboutComponent,
  },
  {
    path: 'vendor',
    component: VendorComponent,
  },
  {
    path: 'contacts',
    component: ContactComponent,
  },
  {
    path: 'careers',
    component: CareersComponent,
  },
  {
    path: 'clients',
    component: ClientComponent,
  },
  {
    path: 'products/:slug',
    component: ProductComponent,
  },
  {
    path: 'gallery',
    component: GalleryComponent,
  },
  {
    path: 'uploader',
    component: UploaderComponent,
  },
  {
    path: 'admin',
    component: AdminLoginComponent,
    canActivate: [AdminGuestGuardService],
  },
  {
    path: 'admin/login',
    component: AdminLoginComponent,
    canActivate: [AdminGuestGuardService],
  },
  {
    path: 'admin/forgot-password',
    component: AdminForgotPasswordComponent,
    canActivate: [AdminGuestGuardService],
  },
  {
    path: 'admin/dashboard',
    component: DashboardComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/sliders',
    component: AdminSlidersComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/add-slider',
    component: AdminAddSliderComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/edit-slider/:banner_id',
    component: AdminAddSliderComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/products',
    component: AdminProductsComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/add-product',
    component: AdminAddProductComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/edit-product/:product_id',
    component: AdminAddProductComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/counters',
    component: AdminCountersComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/add-counter',
    component: AdminAddCounterComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/edit-counter/:counter_id',
    component: AdminAddCounterComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/about',
    component: AdminAboutComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/add-about',
    component: AdminAddAboutComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/edit-about/:about_id',
    component: AdminAddAboutComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/gallery',
    component: AdminGalleryComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/add-gallery',
    component: AdminAddGalleryComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/edit-gallery/:gallery_id',
    component: AdminAddGalleryComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/clients',
    component: AdminClientsComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/add-client',
    component: AdminAddClientComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/edit-client/:client_id',
    component: AdminAddClientComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/testimonials',
    component: AdminTestimonialsComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/add-testimonial',
    component: AdminAddTestimonialComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/edit-testimonial/:testimonial_id',
    component: AdminAddTestimonialComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/careers',
    component: AdminCareersComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/vendor',
    component: AdminVendorComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/contacts',
    component: AdminContactComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/brochure',
    component: AdminBrochureComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/map',
    component: AdminMapComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/add-media',
    component: AdminMediaComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/edit-media/:media_id',
    component: AdminMediaComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/media',
    component: AdminMediaListComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/add-vacancy',
    component: AdminAddVacancyComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/edit-vacancy/:vacancy_id',
    component: AdminAddVacancyComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/vacancy',
    component: AdminVacancyComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/office',
    component: AdminOfficeComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/seo-settings',
    component: AdminSeoComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'admin/sitemap',
    component: AdminSitemapComponent,
    canActivate: [AdminAuthGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
