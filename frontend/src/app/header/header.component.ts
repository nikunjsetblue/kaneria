import { Component, OnInit } from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder} from '@angular/forms';
import {NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router} from '@angular/router';
import {HttpService} from '../service/http.service';
import {LoderService} from '../service/loder.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  showLoader = false;
  abouts = [];
  abouts_id = [];
  products = [];
  products_id = [];
  step:number = 0;
  CurrentUrl = '';
  constructor(private loaderService: LoderService, public toastr: ToastrService, private formBuilder: FormBuilder, public router: Router, public http: HttpService) {
  }

  ngOnInit(): void {
    this.loaderService.isLoaderShown.subscribe(isLoaderShown => this.showLoader = isLoaderShown);
    // this.router.events.subscribe(routerEvent => {
    //   if (routerEvent instanceof NavigationStart) {
    //     this.loaderService.showLoader();
    //   } else if (routerEvent instanceof NavigationEnd) {
    //     this.loaderService.hideLoader();
    //   } else if (routerEvent instanceof NavigationCancel) {
    //     this.loaderService.hideLoader();
    //     // Handle cancel
    //   } else if (routerEvent instanceof NavigationError) {
    //     this.loaderService.hideLoader();
    //     // Handle error
    //   }
    // });
    this.CurrentUrl = this.router.url
    this.about_data();
    this.product_data();
  }

  about_data(){
    var data = {};
    this.loaderService.showLoader();
    this.http.PostAPI('users/GetRecordAbout', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.abouts = resdata.data;
        let abouts_id = [];
        this.abouts.forEach(function (value) {
          abouts_id.push('/about/'+value.about_id);
        });
        this.abouts_id = abouts_id;
        setTimeout(()=> {
          $(document).ready(function($) {
            // @ts-ignore
            jQuery(".mean-menu").meanmenu({ meanScreenWidth: "991" });
          });
        },2000);
        this.loaderService.hideLoader();
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  check_step(step){
    this.step = step;
  }

  product_data(){
    var data = {};
    this.loaderService.showLoader();
    this.http.PostAPI('users/GetRecordProduct', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.products = resdata.data;
        let products_id = [];
        this.products.forEach(function (value) {
          products_id.push('/about/'+value.about_id);
        });
        this.products_id = products_id;
        setTimeout(()=> {
          $(document).ready(function($) {
            // @ts-ignore
            jQuery(".mean-menu").meanmenu({ meanScreenWidth: "991" });
          });
        },2000);
        this.loaderService.hideLoader();
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
