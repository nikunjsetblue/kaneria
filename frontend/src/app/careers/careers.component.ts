import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {HttpService} from '../service/http.service';
import {SeoService} from '../service/seo.service';

@Component({
  selector: 'app-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.css']
})
export class CareersComponent implements OnInit {

  submitted = false;
  AddForm : FormGroup;
  resume_file:File;
  cv = '';
  vacancy = [];
  constructor(public _seoService: SeoService, public toastr: ToastrService, private formBuilder: FormBuilder, public router: Router, public http: HttpService) { }

  ngOnInit(): void {
    this.seo_data();
    this.vacancy_data();
    this.AddForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required,Validators.email]],
      position: ['', [Validators.required]],
      mobile: ['', [Validators.required]],
      city: ['', [Validators.required]],
      state: ['', [Validators.required]],
      past_experience: ['', [Validators.required]],
      past_experience_year: ['', [Validators.required]],
      resume: ['', [Validators.required]],
      recaptcha: ['', [Validators.required]]
    });
  }

  seo_data(){
    var data = {seo_settings_id : 1};
    this.http.PostAPI('users/GetRecordSeoSettings', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this._seoService.updateTitle(resdata.data.careers_meta_title);
        this._seoService.updateKeyword(resdata.data.careers_meta_keyword);
        this._seoService.updateDescription(resdata.data.careers_meta_description);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  get fval() {
    return this.AddForm.controls;
  }

  vacancy_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordVacancy', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.vacancy = resdata.data
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  upload_file(evt) {
    if (evt.target) {
      var ext = evt.target.files[0].name.split('.').pop()
      if(ext=="pdf" || ext=="docx" || ext=="doc") {
        this.resume_file = evt.target.files[0];
        var reader = new FileReader();
        reader.onload = (event: any) => {
          this.cv = event.target.result;
        }
        reader.readAsDataURL(evt.target.files[0]);
      }else{
        this.toastr.error('Please upload valid File');
        this.AddForm.controls['resume'].reset()
      }
    }
  }

  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    let data = this.AddForm.value;
    var form_data  = new FormData()
    for ( var key in data ) {
      form_data.append(key, data[key]);
    }
    if(this.resume_file){
      form_data.append('resume_file',this.resume_file)
    }
    this.http.PostAPI('users/careers', form_data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
        this.submitted = false;
        this.AddForm.reset();
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  alphaOnly(event) {
    var key = event.keyCode;
    return ((key == 32 || key >= 65 && key <= 90) || key == 8);
  }

}
