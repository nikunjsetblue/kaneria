import {Renderer2, Component, OnInit, Inject} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {HttpService} from '../service/http.service';
import {environment} from '../../environments/environment';
import {SeoService} from '../service/seo.service';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  show_model = false;
  show_data = false;
  abouts = []
  products = []
  socials = []
  brochure_name = ''
  brochure_link = ''
  quantity;
  constructor(private _renderer2: Renderer2, @Inject(DOCUMENT) private _document: Document, public _seoService: SeoService, public toastr: ToastrService, private formBuilder: FormBuilder, public router: Router, public http: HttpService) { }

  ngOnInit(): void {
    this.seo_data();
    this.about_data();
    this.product_data();
    this.brochure_data();
    this.social_data();
  }

  seo_data(){
    var data = {seo_settings_id : 1};
    this.http.PostAPI('users/GetRecordSeoSettings', data).then((resdata: any) => {
      if (resdata.status == 200) {
        if(resdata.data.google_analytics != '' && resdata.data.google_analytics != null){
          let script = this._renderer2.createElement('script');
          script.type = 'text/javascript';
          script.innerHTML = `${resdata.data.google_analytics}`;
          this._renderer2.appendChild(this._document.body, script);
        }
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  about_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordAbout', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.abouts = resdata.data;
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  product_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordProduct', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.products = resdata.data;
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  social_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordMedia', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.socials = resdata.data;
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  brochure_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordBrochure', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.brochure_name = resdata.data.brochure_file_name;
        this.brochure_link = (resdata.data.brochure_file) ? environment.backend_url+''+resdata.data.brochure_file : '';
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  calopen(){
    this.show_model = true;
  }

  calclose(){
    this.show_model = false;
    this.quantity = ''
    document.getElementById("cal-data").style.display = "none";
  }

  isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

  calculate(){
    let x = this.quantity;
    if (x) {
      document.getElementById("error").style.display = "none";
      document.getElementById("cal-data").style.display = "block";

      var x1 = x * 0.025315;
      document.getElementById("block").innerHTML = x1.toFixed(2) + ' (CBM)';

      var x2 = x * 0.0189863;
      document.getElementById("bjm").innerHTML = x2.toFixed(2) + ' (Bags of 40 Kg Weight)';

      var x3 = x * 0.326;
      document.getElementById("cement").innerHTML = x3.toFixed(2) + ' (Bags of 50 Kg Weight)';

      var x4 = x * 0.056745;
      document.getElementById("sand").innerHTML = x4.toFixed(2) + ' (MT)';
//1 CFM SAND = 48.5 KG

      var x5 = x * 0.8;
      document.getElementById("steel").innerHTML = x5.toFixed(2) + ' (Kg Reinforcement Steel)';

      var x6 = x * 0.0082566;
      document.getElementById("10mm").innerHTML = x6.toFixed(2) + ' (MT)';
//1 CFM 10MM AGGREGATE = 45.87 KG

      var x7 = x * 0.0144837;
      document.getElementById("20mm").innerHTML = x7.toFixed(2) + ' (MT)';
//1 CFM 20MM AGGREGATE = 43.89 KG

      var x8 = x * 0.01363635;
      document.getElementById("ta").innerHTML = x8.toFixed(2) + ' (Bags of 40 Kg Weight)';

      var x9 = x * 0.005625;
      document.getElementById("putty").innerHTML = x9.toFixed(2) + ' (Bags of 40 Kg Weight)';

    } else {
      document.getElementById("cal-data").style.display = "none";
      document.getElementById("error").style.display = "block";
      document.getElementById("error").innerHTML = "<div class='cal-error'> Please Enter Valid Number!</div>";
    }
  }

}
