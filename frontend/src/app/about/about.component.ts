import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpService} from '../service/http.service';
import {environment} from '../../environments/environment';
import * as $ from 'jquery';
import 'magnific-popup';
import {SeoService} from '../service/seo.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  slug = ''
  about_title = '';
  about_content = '';
  about_content2 = '';
  about_image = '';
  abouts = []
  gallery = []
  backend_url = ''
  loadAPI: Promise<any>;
  constructor(public _seoService: SeoService, public toastr: ToastrService, private formBuilder: FormBuilder, public router: Router, public http: HttpService, private route: ActivatedRoute) {
    route.params.subscribe(params => {
      this.slug = params.slug
      if(this.slug != ''){
        this.about_data();
      }
      setTimeout(()=>{
        $(document).ready(function($) {
          // @ts-ignore
          $('.popup-gallery').magnificPopup({
            delegate: "a",
            type: "image",
            tLoading: "Loading image #%curr%...",
            mainClass: "mfp-img-mobile",
            gallery: {enabled: true, navigateByImgClick: true, preload: [0, 1]},
          });
        });
      },2000)
    })
  }

  ngOnInit (): void {
    this.backend_url = environment.backend_url;
    this.about_all_data()
    this.gallery_data()
  }

  about_all_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordAbout', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.abouts = resdata.data
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  gallery_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordGallery', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.gallery = resdata.data
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  about_data(){
    var data = {slug:this.slug};
    this.http.PostAPI('users/GetRecordAbout', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.about_title = resdata.data[0].about_title;
        this.about_content = resdata.data[0].about_content;
        this._seoService.updateTitle(resdata.data[0].meta_title);
        this._seoService.updateKeyword(resdata.data[0].meta_keyword);
        this._seoService.updateDescription(resdata.data[0].meta_description);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
