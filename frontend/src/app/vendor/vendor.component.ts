import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SeoService} from '../service/seo.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {HttpService} from '../service/http.service';

@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.css']
})
export class VendorComponent implements OnInit {

  submitted = false;
  AddForm : FormGroup;
  products = [];
  quotation_file:File;
  quotation = '';
  constructor(public _seoService: SeoService, public toastr: ToastrService, private formBuilder: FormBuilder, public router: Router, public http: HttpService) { }

  ngOnInit(): void {
    this.seo_data();
    this.product_data();
    this.AddForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required,Validators.email]],
      pincode: ['', [Validators.required]],
      mobile: ['', [Validators.required]],
      city: ['', [Validators.required]],
      state: ['', [Validators.required]],
      product_id: ['', [Validators.required]],
      quotation: [''],
      recaptcha: ['', Validators.required]
    });
  }

  alphaOnly(event) {
    var key = event.keyCode;
    return ((key == 32 || key >= 65 && key <= 90) || key == 8);
  }

  product_data(){
    var data = {};
    this.http.PostAPI('users/GetRecordProduct', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.products = resdata.data
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  upload_file(evt) {
    if (evt.target) {
      var ext = evt.target.files[0].name.split('.').pop()
      if(ext=="pdf" || ext=="docx" || ext=="doc") {
        this.quotation_file = evt.target.files[0];
        var reader = new FileReader();
        reader.onload = (event: any) => {
          this.quotation = event.target.result;
        }
        reader.readAsDataURL(evt.target.files[0]);
      }else{
        this.toastr.error('Please upload valid File');
        this.AddForm.controls['quotation'].reset()
      }
    }
  }

  seo_data(){
    var data = {seo_settings_id : 1};
    this.http.PostAPI('users/GetRecordSeoSettings', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this._seoService.updateTitle(resdata.data.vendor_meta_title);
        this._seoService.updateKeyword(resdata.data.vendor_meta_keyword);
        this._seoService.updateDescription(resdata.data.vendor_meta_description);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  get fval() {
    return this.AddForm.controls;
  }

  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    let data = this.AddForm.value;
    var form_data  = new FormData()
    for ( var key in data ) {
      form_data.append(key, data[key]);
    }
    if(this.quotation_file){
      form_data.append('quotation_file',this.quotation_file)
    }
    this.http.PostAPI('users/vendor', form_data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
        this.submitted = false;
        this.AddForm.reset();
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
