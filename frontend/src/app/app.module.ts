import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import {RouterModule} from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AppRoutingModule } from './app-routing.module';
import { CarouselModule } from 'ngx-owl-carousel-o';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
// @ts-ignore
import { NgxOdometerModule } from 'ngx-odometer';
import { AboutComponent } from './about/about.component';
import { CareersComponent } from './careers/careers.component';
import { ContactComponent } from './contact/contact.component';
import { ProductComponent } from './product/product.component';
import { GalleryComponent } from './gallery/gallery.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { AdminForgotPasswordComponent } from './admin/admin-forgot-password/admin-forgot-password.component';
import { AdminHeaderComponent } from './admin/admin-header/admin-header.component';
import { AdminSidebarComponent } from './admin/admin-sidebar/admin-sidebar.component';
import { AdminFooterComponent } from './admin/admin-footer/admin-footer.component';
import { AdminAfterFooterComponent } from './admin/admin-after-footer/admin-after-footer.component';
import { AdminSlidersComponent } from './admin/admin-sliders/admin-sliders.component';
import { AdminAddSliderComponent } from './admin/admin-add-slider/admin-add-slider.component';
import { AdminProductsComponent } from './admin/admin-products/admin-products.component';
import { AdminAddProductComponent } from './admin/admin-add-product/admin-add-product.component';
import { AdminAboutComponent } from './admin/admin-about/admin-about.component';
import { AdminAddAboutComponent } from './admin/admin-add-about/admin-add-about.component';
import { AdminClientsComponent } from './admin/admin-clients/admin-clients.component';
import { AdminAddClientComponent } from './admin/admin-add-client/admin-add-client.component';
import { AdminAddTestimonialComponent } from './admin/admin-add-testimonial/admin-add-testimonial.component';
import { AdminTestimonialsComponent } from './admin/admin-testimonials/admin-testimonials.component';
import { AdminCountersComponent } from './admin/admin-counters/admin-counters.component';
import { AdminAddCounterComponent } from './admin/admin-add-counter/admin-add-counter.component';
//import {CKEditorModule} from 'ckeditor4-angular';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { EditorModule } from '@tinymce/tinymce-angular';
import { DataTablesModule } from 'angular-datatables';
import { AdminCareersComponent } from './admin/admin-careers/admin-careers.component';
import { AdminContactComponent } from './admin/admin-contact/admin-contact.component';
import { AdminBrochureComponent } from './admin/admin-brochure/admin-brochure.component';
import { AdminMediaComponent } from './admin/admin-media/admin-media.component';
import { AdminMediaListComponent } from './admin/admin-media-list/admin-media-list.component';
import { AdminGalleryComponent } from './admin/admin-gallery/admin-gallery.component';
import { AdminAddGalleryComponent } from './admin/admin-add-gallery/admin-add-gallery.component';
import { AdminMapComponent } from './admin/admin-map/admin-map.component';
import { AdminVacancyComponent } from './admin/admin-vacancy/admin-vacancy.component';
import { AdminAddVacancyComponent } from './admin/admin-add-vacancy/admin-add-vacancy.component';
import { ClientComponent } from './client/client.component';
import { AdminSeoComponent } from './admin/admin-seo/admin-seo.component';
import { AdminSitemapComponent } from './admin/admin-sitemap/admin-sitemap.component';
import { VendorComponent } from './vendor/vendor.component';
import { AdminVendorComponent } from './admin/admin-vendor/admin-vendor.component';
import { UploaderComponent } from './uploader/uploader.component';
import {RECAPTCHA_SETTINGS, RecaptchaFormsModule, RecaptchaModule, RecaptchaSettings} from 'ng-recaptcha';
import { AdminOfficeComponent } from './admin/admin-office/admin-office.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    AboutComponent,
    CareersComponent,
    ContactComponent,
    ProductComponent,
    GalleryComponent,
    DashboardComponent,
    AdminLoginComponent,
    AdminForgotPasswordComponent,
    AdminHeaderComponent,
    AdminSidebarComponent,
    AdminFooterComponent,
    AdminAfterFooterComponent,
    AdminSlidersComponent,
    AdminAddSliderComponent,
    AdminProductsComponent,
    AdminAddProductComponent,
    AdminAboutComponent,
    AdminAddAboutComponent,
    AdminClientsComponent,
    AdminAddClientComponent,
    AdminAddTestimonialComponent,
    AdminTestimonialsComponent,
    AdminCountersComponent,
    AdminAddCounterComponent,
    AdminCareersComponent,
    AdminContactComponent,
    AdminBrochureComponent,
    AdminMediaComponent,
    AdminMediaListComponent,
    AdminGalleryComponent,
    AdminAddGalleryComponent,
    AdminMapComponent,
    AdminVacancyComponent,
    AdminAddVacancyComponent,
    ClientComponent,
    AdminSeoComponent,
    AdminSitemapComponent,
    VendorComponent,
    AdminVendorComponent,
    UploaderComponent,
    AdminOfficeComponent
  ],
    imports: [
      BrowserModule,
      AppRoutingModule,
      RouterModule,
      HttpClientModule,
      BrowserAnimationsModule,
      DataTablesModule,
      ReactiveFormsModule,
      FormsModule,
      CarouselModule,
      CKEditorModule,
      EditorModule,
      RecaptchaModule,
      RecaptchaFormsModule,
      ToastrModule.forRoot(),
      NgxOdometerModule.forRoot()
    ],
  providers: [{
    provide: RECAPTCHA_SETTINGS,
    useValue: {
      siteKey: '6LckQiAbAAAAACtOhvrlY92o11E4YQm1lBjPLhoF',
    } as RecaptchaSettings,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
