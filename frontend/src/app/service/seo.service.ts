import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class SeoService {

  constructor(private title: Title, private meta: Meta) { }

  updateTitle(title: string) {
    this.title.setTitle(title);
  }

  updateKeyword(keyword: string) {
    this.meta.updateTag({ name: 'keyword', content: keyword })
  }

  updateDescription(description: string) {
    this.meta.updateTag({ name: 'description', content: description })
  }

}
