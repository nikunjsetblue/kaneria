jQuery(
  (function ($) {
    "use strict";
    $(window).on("scroll", function () {
      if ($(this).scrollTop() > 50) {
        $(".main-nav").addClass("menu-shrink");
      } else {
        $(".main-nav").removeClass("menu-shrink");
      }
    });
    $(".search-toggle").addClass("closed");
    $(".search-toggle .search-icon").on("click", function (e) {
      if ($(".search-toggle").hasClass("closed")) {
        $(".search-toggle").removeClass("closed").addClass("opened");
        $(".search-toggle, .search-area").addClass("opened");
        $("#search-terms").focus();
      } else {
        $(".search-toggle").removeClass("opened").addClass("closed");
        $(".search-toggle, .search-area").removeClass("opened");
      }
    });
   $(".modal a")
      .not(".dropdown-toggle")
      .on("click", function () {
        $(".modal").modal("hide");
      });

    $(".odometer").appear(function (e) {
      var odo = $(".odometer");
      odo.each(function () {
        var countNumber = $(this).attr("data-count");
        $(this).html(countNumber);
      });
    });
    $("select").niceSelect();

    $(".progress-bar").loading();
    $(".accordion > li:eq(0) a").addClass("active").next().slideDown();
    $(".accordion a").on("click", function (j) {
      var dropDown = $(this).closest("li").find("p");
      $(this).closest(".accordion").find("p").not(dropDown).slideUp();
      if ($(this).hasClass("active")) {
        $(this).removeClass("active");
      } else {
        $(this).closest(".accordion").find("a.active").removeClass("active");
        $(this).addClass("active");
      }
      dropDown.stop(false, true).slideToggle();
      j.preventDefault();
    });
    let getDaysId = document.getElementById("days");
    if (getDaysId !== null) {
      const second = 1000;
      const minute = second * 60;
      const hour = minute * 60;
      const day = hour * 24;
      let countDown = new Date("December 30, 2021 00:00:00").getTime();
      setInterval(function () {
        let now = new Date().getTime();
        let distance = countDown - now;
        (document.getElementById("days").innerText = Math.floor(distance / day)),
          (document.getElementById("hours").innerText = Math.floor((distance % day) / hour)),
          (document.getElementById("minutes").innerText = Math.floor((distance % hour) / minute)),
          (document.getElementById("seconds").innerText = Math.floor((distance % minute) / second));
      }, second);
    }
    $("body").append('<div id="toTop" class="back-to-top-btn"><i class="bx bx-up-arrow-alt"></i></div>');
    //$("body").append('<a href="https://wa.me/9978988125" target="_blank"><div class="whatsapp-icon"><i class="bx bxl-whatsapp"></i></div></a>');
    $(window).on("scroll", function () {
      if ($(this).scrollTop() != 0) {
        $("#toTop").fadeIn();
      } else {
        $("#toTop").fadeOut();
      }
    });
    $("#toTop").on("click", function () {
      $("html, body").animate({ scrollTop: 0 }, 900);
      return false;
    });

  })(jQuery)
);
