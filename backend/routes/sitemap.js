const { check, validationResult } = require('express-validator');
const conn = require('../db/connections');
const express = require("express");
const router = express.Router();
const js2xmlparser = require("js2xmlparser");
const moment = require("moment");
var FRONTEND_URL = process.env.FRONTEND_URL;

/**
 * It generates a standard sitemal.xml for SEO purposes
 */
router.get("/", async function(req, res, next) {
  let schemalocation;
  try {
    const abouts = await getRecordsAbout();
    const products = await getRecordsProducts();
    const general = await getRecordsGeneral();
    //our records to index
    const collection = [];
    let today = moment();
    today = today.format("YYYY-MM-DD");
    //add site root url
    const rootUrl = {};
    rootUrl.loc = FRONTEND_URL;
    //rootUrl.lastmod = today;
    //rootUrl.changefreq = "daily";
    //rootUrl.priority = "1.0";
    await collection.push(rootUrl);

    //add recipes urls
    if (abouts.length > 0) {
      for (let i = 0; i < abouts.length; i++) {
        const url = {};
        url.loc = abouts[i].url;
        //url.lastmod = abouts[i].updated_at;
        await collection.push(url);
      }
    }
    if (products.length > 0) {
      for (let i = 0; i < products.length; i++) {
        const url = {};
        url.loc = products[i].url;
        //url.lastmod = products[i].updated_at;
        await collection.push(url);
      }
    }
    if (general.length > 0) {
      for (let i = 0; i < general.length; i++) {
        const url = {};
        url.loc = general[i].url;
        //url.lastmod = general[i].updated_at;
        await collection.push(url);
      }
    }
    const col = {
      "@": {
        xsi:schemalocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
      },
      url: collection,
    };
    const xml = js2xmlparser.parse("urlset", col);
    res.set("Content-Type", "text/xml");
    res.status(200);
    res.send(xml);
  } catch (e) {
    next(e);
  }
});

/**
 * @return a collection to index (typically we'll get these records from our database)
 */
async function getRecordsProducts() {
  //these records will have our own structure, we return as they are and later we convert them to the xml standard format
  //so let's just define two records hard-coded
  return new Promise((resolve, reject) => {
    let records = [];
    conn.query('SELECT slug,created_at FROM products ORDER BY position', (error, resdata) => {
      if(error){
        res.json({status: 400, message: error});
      }else{
        if(resdata.length > 0){
          resdata.forEach(item => {
            let product = {
              url : FRONTEND_URL+'products/'+item.slug,
              //updated_at : moment(item.created_at).format('YYYY-MM-DD')
            }
            records.push(product);
          });
        }
      }
      resolve(records);
    });
  });
}

async function getRecordsAbout() {
  //these records will have our own structure, we return as they are and later we convert them to the xml standard format
  //so let's just define two records hard-coded
  return new Promise((resolve, reject) => {
    let records = [];
    conn.query('SELECT slug,created_at FROM about', (error, resdata) => {
      if(error){
        res.json({status: 400, message: error});
      }else{
        if(resdata.length > 0){
          resdata.forEach(item => {
            let about = {
              url : FRONTEND_URL+'about/'+item.slug,
              //updated_at : moment(item.created_at).format('YYYY-MM-DD')
            }
            records.push(about);
          });
        }
      }
      resolve(records);
    });
  });
}

async function getRecordsGeneral() {
  //these records will have our own structure, we return as they are and later we convert them to the xml standard format
  //so let's just define two records hard-coded
  return new Promise((resolve, reject) => {
    let records = [];
    let today = moment();
    today = today.format("YYYY-MM-DD");
    let vendor = {
      url : FRONTEND_URL+'vendor',
      //updated_at : today
    }
    records.push(vendor);
    let careers = {
      url : FRONTEND_URL+'careers',
      //updated_at : today
    }
    records.push(careers);
    let clients = {
      url : FRONTEND_URL+'clients',
      //updated_at : today
    }
    records.push(clients);
    let contacts = {
      url : FRONTEND_URL+'contacts',
      //updated_at : today
    }
    records.push(contacts);
    resolve(records);
  });
}
module.exports = router;