var express = require('express');
var router = express.Router();
const { check, validationResult } = require('express-validator');
const conn = require('../db/connections');
const helper = require("../helper/helper");
const request = require('request');
const fs = require("fs");
const path = require("path");
var BACKEND_URL = process.env.BACKEND_URL;
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/GetRecordBanner',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    let sql = "SELECT * FROM `home_silders` ORDER BY `position`";
    conn.query(sql, (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get Data Successfully', data: results});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/GetRecordSeoSettings',[
  check('seo_settings_id').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    var seo_settings_id = req.body.seo_settings_id;
    let sql = "SELECT * FROM `seo_settings` WHERE seo_settings_id = ?";
    conn.query(sql, [seo_settings_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get data successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/GetRecordVacancy',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    let sql = "SELECT * FROM `opportunities` ORDER BY `created_at`";
    conn.query(sql, (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get Data Successfully', data: results});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/GetRecordCounter',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    let sql = "SELECT * FROM `counters`";
    conn.query(sql, (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get Data Successfully', data: results});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/GetRecordProduct',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    let slug =  req.body.slug;
    let where = '';
    if(slug != '' && slug != null && slug != undefined){
      where += " WHERE `slug` = '"+slug+"'";
    }else{
      where += ' ORDER BY `position`';
    }
    let sql = "SELECT * FROM `products`"+where;
    conn.query(sql, (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get Data Successfully', data: results});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/GetRecordMap',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    let sql = "SELECT * FROM `map`";
    conn.query(sql, (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get Data Successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/GetRecordAddress',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    let sql = "SELECT * FROM `address`";
    conn.query(sql, (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get Data Successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/GetRecordMedia',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    let media_id =  req.body.media_id;
    let where = '';
    if(media_id != '' && media_id != null && media_id != undefined){
      where += ' WHERE `media_id` = '+media_id;
    }
    let sql = "SELECT * FROM `media`"+where;
    conn.query(sql, (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get Data Successfully', data: results});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/GetRecordBrochure',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    let brochure_id =  req.body.brochure_id;
    let where = '';
    if(brochure_id != '' && brochure_id != null && brochure_id != undefined){
      where += ' WHERE `brochure_id` = '+brochure_id;
    }
    let sql = "SELECT * FROM `brochure`"+where;
    conn.query(sql, (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          let brochure_file_name = results[0].brochure_file.split('/').pop()
          results[0].brochure_file_name = brochure_file_name
          res.json({status: 200, message: 'Get Data Successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/GetRecordAbout',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    let slug = req.body.slug;
    let where = '';
    if(slug != '' && slug != null && slug != undefined){
      where += " WHERE `slug`='"+slug+"'";
    }else{
      where += ' ORDER BY `position`';
    }
    let sql = "SELECT * FROM `about`"+where;
    conn.query(sql, (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get Data Successfully', data: results});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/GetRecordGallery',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    let sql = "SELECT * FROM `gallery` ORDER BY `position`";
    conn.query(sql, (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get Data Successfully', data: results});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/GetRecordClient',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    let sql = "SELECT * FROM `clients`";
    conn.query(sql, (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get Data Successfully', data: results});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/GetRecordTestimonials',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    let sql = "SELECT * FROM `testimonials`";
    conn.query(sql, (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get Data Successfully', data: results});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/contacts',[
  check('name').exists(),
  check('email').exists().isEmail(),
  check('mobile').exists(),
  check('product_id').exists(),
  check('message').exists()
], (req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 401, message: err});
    return
  }else {
    var {name,email,mobile,product_id,message} = req.body
    email = email.toLowerCase()
    let data = {
      name:name,
      email:email,
      mobile:mobile,
      product_id:product_id,
      message:message,
    }
    let sql1 = "INSERT INTO contacts SET ?";
    conn.query(sql1, data, (err, results1) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results1.affectedRows === 1) {
          var html = "";
          html += `<h1>Contact Us For Kaneria Plast</h1>`;
          html += `You are now a contact for Kaneria Plast <br>`;
          html += `Name: ${name} <br>`;
          html += `Mobile: ${mobile} <br>`;
          html += `Message: ${message}`;
          var subject = "Contact us";
          var check  = helper.email_helper('',email,subject,html)
          if (check) {
            res.json({status: 200, message: 'Thank you for subscribing!'});
          }else{
            res.json({status: 400, message: 'Thank you for subscribing!'});
          }
        } else {
          res.json({status: 400, message: 'Contact form is Failed'});
        }
      }
    });
  }
});

router.post('/careers',[
  check('name').exists(),
  check('email').exists().isEmail(),
  check('mobile').exists(),
  check('position').exists(),
  check('city').exists(),
  check('state').exists()
], (req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 401, message: err});
    return
  }else {
    var {name,email,mobile,position,city,state,past_experience,past_experience_year} = req.body
    email = email.toLowerCase();
    var attachments;
    let data = {
      name:name,
      email:email,
      mobile:mobile,
      position:position,
      city:city,
      state:state,
      past_experience:past_experience,
      past_experience_year:past_experience_year
    }
    if (req.files && Object.keys(req.files).length > 0) {
      if (req.files.resume_file != undefined) {
        let resume = req.files.resume_file;
        resume.name = resume.name.replace(/ /g, "_");
        let filename1 = Math.floor(Math.random() * 100000) + '-' + resume.name;
        let file_url1 = './upload/resume/' + filename1;
        let file_name1 = 'upload/resume/' + filename1;
        resume.mv(file_url1, function (error) {
          if (error) {
            res.json({status: 400, message: error});
          } else {
            data.resume = file_name1;
            attachments = [{filename: filename1, path: BACKEND_URL +''+ file_name1}];
            let sql1 = "INSERT INTO careers SET ?";
            conn.query(sql1, data, (err, results1) => {
              if (err) {
                res.json({status: 400, message: err});
              } else {
                if (results1.affectedRows === 1) {
                  var html = "";
                  html += `<h1>Careers For Kaneria Plast</h1>`;
                  html += `You are now a Careers for Kaneria Plast <br>`;
                  html += `Name: ${name} <br>`;
                  html += `Mobile: ${mobile} <br>`;
                  html += `Position: ${position} <br>`;
                  html += `City: ${city} <br>`;
                  html += `State: ${state}`;
                  var subject = "Job Opportunity";
                  var to = 'hr@kaneriaplast.com';
                  var check  = helper.email_helper('', to, subject, html, attachments);
                  if (check) {
                    res.json({status: 200, message: 'Thank you for applied opportunity!'});
                  }else{
                    res.json({status: 400, message: 'Thank you for applied opportunity!'});
                  }
                } else {
                  res.json({status: 400, message: 'Opportunity form is failed'});
                }
              }
            });
          }
        });
      }
    }
  }
});

router.post('/vendor',[
  check('name').exists(),
  check('email').exists().isEmail(),
  check('mobile').exists(),
  check('pincode').exists(),
  check('city').exists(),
  check('state').exists(),
  check('product_id').exists()
], (req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 401, message: err});
    return
  }else {
    var {name,email,mobile,pincode,city,state,product_id} = req.body
    email = email.toLowerCase()
    var attachments;
    let data = {
      name:name,
      email:email,
      mobile:mobile,
      pincode:pincode,
      city:city,
      state:state,
      product_id:product_id
    }
    if (req.files && Object.keys(req.files).length > 0) {
      if (req.files.quotation_file != undefined) {
        let quotation = req.files.quotation_file;
        quotation.name = quotation.name.replace(/ /g, "_");
        let filename1 = Math.floor(Math.random() * 100000) + '-' + quotation.name;
        let file_url1 = './upload/quotation/' + filename1;
        let file_name1 = 'upload/quotation/' + filename1;
        quotation.mv(file_url1, function (error) {
          if (error) {
            res.json({status: 400, message: error});
          } else {
            data.quotation = file_name1;
            attachments = [{filename: filename1, path: BACKEND_URL +''+ file_name1}];
          }
        });
      }
    }
    let sql1 = "INSERT INTO vendor SET ?";
    conn.query(sql1, data, async (err, results1) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results1.affectedRows === 1) {
          var html = "";
          html += `<h1>Become Vendor For Kaneria Plast</h1>`;
          html += `You are now a Vendor for Kaneria Plast <br>`;
          html += `Name: ${name} <br>`;
          html += `Mobile: ${mobile} <br>`;
          html += `Pincode: ${pincode} <br>`;
          html += `City: ${city} <br>`;
          html += `State: ${state}  <br>`;
          html += `Product: ${product_id} <br>`;
          var subject = "Become Vendor";
          var to = ['plant@kaneriaplast.com', 'Kaneriagroup@gmail.com'];
          var check = helper.email_helper('', to, subject, html, attachments);
          if (check) {
            res.json({status: 200, message: 'Thank you for applied become vendor!'});
          } else {
            res.json({status: 400, message: 'Thank you for applied become vendor!'});
          }
        } else {
          res.json({status: 400, message: 'vendor form is failed'});
        }
      }
    });
  }
});

module.exports = router;
