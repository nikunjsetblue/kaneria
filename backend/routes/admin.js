var express = require('express');
var router = express.Router();
const { check, validationResult } = require('express-validator');
const conn = require('../db/connections');
const SitemapGenerator = require('sitemap-generator');

const jwt = require('jsonwebtoken');
var crypto = require('crypto');
const helper = require("../helper/helper");
var JWTSECRET = process.env.ADMINJWTSECRET;
var FRONTEND_URL = process.env.FRONTEND_URL;
var BACKEND_URL = process.env.BACKEND_URL;

const privateKey = `
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAwM4k/yLdM1ur0RiEo+ocpWw6Nz6mdhsfgPut9hniZHxKilvt
TXMHs/zlMDAv10g05OKJlTsMaxrg+2QnewpnX/HvmLfoQNGxmRu6Efcwh7wtewLh
gIA5Zlq5gEWaRn2+shcUZ4nVywAY1qK8aCqUj1RBQR4M78vnsUb/otokbFzwL/wA
7+IYLDXZgVDYOROnrbw81bW9Eo/oMRKlU5S8Yh81FuT3XqB1y9BoItxw/1wW/X6k
8wbkLZx7CObo1KfGVcQYwHi1Fn28tn+ks7wkLCZeOpSGyyJiJzIHmo97/9wSuBq6
45CYfN6bpLnZUtyJAWbBAterv1SQstKGboHuDwIDAQABAoIBAB6M65bT3uoC5hRB
Rs80Suq5XfYgAr9nJ9RHzPZYfqN+gE8xJsZlajyOX4CFuibqJZt4++T+EMct4KVk
5s6bxOqSiu0Pgwk2UOZaWt765KE1Y52AxNuYNcAuswMMxFfUF/HfL1gCorJCZy0n
ZyQ4TwbxCk/3o4QhsqpH/jy/RRM4Vh5NbSVeXo97qHACtPO6bEfJ2v4yIDKpsEyE
ICLBXOT0zUsKYye6MZuXnbiY8WjSvFvz/uJgiqRLS7qYNs4Vgbl5nhDkIGeVUs/j
sFqZ6Kt4JAwax9+kAuOIXGmH2rPLEC4biLXpUjo/1vDIzGwfu07GfVfXSyAJHmib
t+4C3ukCgYEA8WsaNytHrLyFKM5Rbgwuzwb+CjN5Hg3EmTq2FdMZgSaDCYIaU/kD
D+DB4K4IkFJTlqa+5JeGdrdT6p8FaLStfIcTQAPdjCWS1XDTRUjGQhNeoUA3ed8T
UA+K6R8fJnlcCOM8T6X0xUBGhZRLLVnVEkYx727ElmQU77X1VySyJPkCgYEAzHNe
XnMzlcerEk4eHrExUSVNulSBdwBPbrWQuIBVX9VZIozTEIEhXXFxOn/eJSQAM44f
DFaie+mIn0HCzHo+/54E4nyHzPvFb0werbr/3FGh+IG+r43ahiD9Y/qslyh1RB4E
y1963KCeUYjNgN7VHQ8aPaIDD3TbKRYnaU0rVUcCgYEAlBaXwSm6ULpfkFVm2/N7
LyFh6c4DSK03YKsEEMUqSu/dx5JYJhSEjVPYecMnm6f0gGAOjthn8iCoyC39umnC
VbXh2A3G8/YEaQtJfaGTg+qvzIhAv3BYP28ZTcD6cbZV/1msoErzp5+y4rPBnbMS
IaCPdwjibEI+GYDrWyY2ZLkCgYBn7u42TZIXvl00/QlKyQEtIy2UX4lBGaNHwH1F
jdfY0/hwG+7gIQT5H8ZT2tTvRJHlo4YdG52WKG4Pp7qDgFGuWIJbMLtIdWU04xll
b/7J3aXdUvfA/vfr37FwteovHZ4fdrMxP+SRbAWPpV/y91SXZSoN0uKDZQBUrrJy
UYyJ2QKBgQDOm5xjNsSUmxF67GLXAg2tIubmkg0K+Vwd/cO9X3zpdUkUoNoqXI8P
Jz2q6X3sY+A06GFppCHngczi4Y2wwSkSvvU4o/zhkYa94V6/whlysES4tWWh8WDt
/pi1zXt7Vfq6zihzk30T95+/JBZWFkuu4dpKQsl+Stj2JyfnbZQBhQ==
-----END RSA PRIVATE KEY-----
`;

router.post("/jwt", function (req, res) {
  const payload = {
    sub: "987654321", // Unique user id string
    name: "nikunj hapani", // Full name of user
    exp: Math.floor(Date.now() / 1000) + 60 * 10, // 10 minutes expiration
  };
  try {
    const token = jwt.sign(payload, privateKey, { algorithm: "RS256" });
    res.json({status: 200, token: token});
  } catch (e) {
    res.status(500);
    res.send(e.message);
  }
});

router.post('/login', [
  check('email').exists(),
  check('password').exists()
], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 401, message: err});
    return
  }else {
    var { email, password } = req.body
    conn.query('SELECT * FROM `admin` WHERE email = ? and password = ?', [email, crypto.createHash('sha256').update(password, 'utf8').digest('hex')], async(err, resuser) => {
      if (err) {
        res.json({status: 501, message: err});
      } else if (resuser.length > 0) {
        var jwtdata = {
          admin_id: resuser[0].admin_id,
          firstname: resuser[0].firstname,
          lastname: resuser[0].lastname,
          email: resuser[0].email
        }
        var token = jwt.sign({ jwtdata, IsAdminLogin: true }, JWTSECRET, { expiresIn: '10d' });
        res.json({status: 200, message: 'Welcome', data: resuser[0], token: token, IsAdminLogin: true});
      } else {
        res.json({status: 201, message: 'Invalid credentials.'});
      }
    })
  }
});

router.post('/forgot-password',[
  check('email').exists().isEmail(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 401, message: err});
    return
  }else {
    const {email} = req.body;
    var verify_token = Math.floor(1000 + Math.random() * 9000);
    let sql = "UPDATE `admin` SET auth_token = ? WHERE email = ?";
    conn.query(sql, [verify_token,email], (err, results) => {
      if (err) {
        res.json({status: 501, message: err});
      } else {
        if (results.affectedRows === 1) {
          var html = "";
          html += `<h1>Forgot your password For Kaneria Plast</h1>`;
          html += `You are now a admin for Kaneria Plast <br>`;
          html += `<br> Token is : ${verify_token}`;
          let subject = "Forgot Password Admin"
          let check  = helper.email_helper('',email,subject,html)
          if (check){
            res.json({status: 200, message: 'Email Send Successfully'});
          }else {
            res.json({status: 400, message: 'Email not sending'});
          }
        } else {
          res.json({status: 400, message: 'No data found'});
        }
      }
    });
  }
});

router.post('/verify-otp',[
  check('email').exists().isEmail(),
  check('verify_token').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 401, message: err});
    return
  }else {
    const {email,verify_token} = req.body;
    let sql = "SELECT * FROM `admin` WHERE email = ? LIMIT 1";
    conn.query(sql, [email], (err, results) => {
      if (err) {
        res.json({status: 501, message: err});
      } else {
        if (results.length > 0){
          if (results[0].auth_token == verify_token){
            res.json({status: 200, message: 'OTP Verify Successfully'});
          }else {
            res.json({status: 400, message: 'Please Valid OTP'});
          }
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/reset-password',[
  check('password').exists().isLength({ min: 6 }).withMessage('must be at least 6 chars long'),
  check('confirm_password').exists().isLength({ min: 6 }).withMessage('must be at least 6 chars long'),
  check('email').exists().isEmail(),
  check('verify_token').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 401, message: err});
    return
  }else {
    const password = req.body.password;
    const confirm_password = req.body.confirm_password;
    const email = req.body.email;
    const auth_token = req.body.verify_token;
    if(password !== confirm_password){
      res.json({status: 400, message: 'Do not match Password and Confirm Password'});
    }
    let sql = "SELECT * FROM `admin` WHERE email = ? AND auth_token = ? LIMIT 1";
    conn.query(sql, [email,auth_token], (err, results) => {
      if (err) {
        res.json({status: 501, message: err});
      } else {
        if (results.length > 0){
          let sql1 = "UPDATE `admin` SET password = ? WHERE email = ?";
          conn.query(sql1, [crypto.createHash('sha256').update(password, 'utf8').digest('hex'),email], function (err, results1) {
            if (err) {
              res.json({status: 501, message: err});
            } else {
              if (results1.affectedRows === 1){
                res.json({status: 200, message: "Your password has been successfully changed"});
              }else{
                res.json({status: 400, message: "Your password has been not changed"});
              }
            }
          });
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/get_data',[
  //passport.authenticate('adminLogin', { session: false }),
  check('admin_id').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    var admin_id = req.body.admin_id;
    let sql = "SELECT * FROM `admin` WHERE admin_id = ?";
    conn.query(sql, [admin_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get Data Successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/addProduct',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    let {product_id,position,title,description,benefits,specifications,warning,meta_title,meta_keyword,meta_description} = req.body;
    let slug = title
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'')
    ;
    let data = {
      title:title,
      position:position,
      slug:slug,
      description:description,
      benefits:benefits,
      specifications:specifications,
      warning:warning,
      meta_title:meta_title,
      meta_keyword:meta_keyword,
      meta_description:meta_description
    }

    if (req.files && Object.keys(req.files).length > 0) {
      if (req.files.image != undefined) {
        let image = req.files.image;
        image.name = image.name.replace(/ /g,"_");
        let filename = Math.floor(Math.random() * 100000) + '-' + image.name;
        let file_url = './upload/product/' + filename;
        let file_name = 'upload/product/' + filename;
        image.mv(file_url, function (error) {
          if (error) {
            res.json({status: 400, message: error});
          } else {
            data.image = file_name;
          }
        });
      }
      if (req.files.cover_image != undefined) {
        let cover_image = req.files.cover_image;
        cover_image.name = cover_image.name.replace(/ /g,"_");
        let filename1 = Math.floor(Math.random() * 100000) + '-' + cover_image.name;
        let file_url1 = './upload/product/' + filename1;
        let file_name1 = 'upload/product/' + filename1;
        cover_image.mv(file_url1, function (error) {
          if (error) {
            res.json({status: 400, message: error});
          } else {
            data.cover_image = file_name1;
          }
        });
      }
      if (req.files.mobile_cover_image != undefined) {
        let mobile_cover_image = req.files.mobile_cover_image;
        mobile_cover_image.name = mobile_cover_image.name.replace(/ /g,"_");
        let filename2 = Math.floor(Math.random() * 100000) + '-' + mobile_cover_image.name;
        let file_url2 = './upload/product/' + filename2;
        let file_name2 = 'upload/product/' + filename2;
        mobile_cover_image.mv(file_url2, function (error) {
          if (error) {
            res.json({status: 400, message: error});
          } else {
            data.mobile_cover_image = file_name2;
          }
        });
      }
    }
    let sql = "SELECT * FROM `products` WHERE product_id = ?";
    conn.query(sql, [product_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          let sql = "UPDATE `products` SET ? WHERE product_id = ?";
          conn.query(sql, [data, product_id], (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Product update successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }else{
          let sql = "INSERT INTO `products` SET ?";
          conn.query(sql, data, (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Product created successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }
      }
    });
  }
});

router.post('/GetProducts',[
  check('columns').exists(),
  check('start').exists(),
  check('order').exists(),
  check('length').exists()
],(req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.status(201).json({
      success: false,
      message: err
    });
    return;
  } else {
    var { columns, order, start, length } = req.body;
    let sql = `SELECT * FROM products ORDER BY ${columns[order[0].column].data} ${order[0].dir} LIMIT ${start},${length};SELECT count(*) as cnt FROM products`;
    conn.query(sql, (error, resdata) => {
      if(error){
        res.json({status: 400, message: error});
      }else{
        if(resdata[0].length > 0){
          res.json({status: 200, message: "Get data successfully", response: resdata[0], TotalRecords:resdata[1][0]});
        }else{
          res.json({status: 400, message: 'No data found'});
        }
      }
    });
  }
});

router.post('/GetRecordProduct',[
  check('product_id').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    var product_id = req.body.product_id;
    let sql = "SELECT * FROM `products` WHERE product_id = ?";
    conn.query(sql, [product_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get data successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/removeProduct',[
  check('product_id').exists()
], function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    var product_id = req.body.product_id;
    let sql = "DELETE FROM `products` WHERE product_id = ?";
    conn.query(sql, [product_id], function (err, results) {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        res.json({status: 200, message: "Product removed successfully"});
      }
    });
  }
});

router.post('/addSlider',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    let {banner_id,position,link} = req.body;

    let data = {
      link : link,
      position : position,
      created_at : new Date()
    };

    if (req.files && Object.keys(req.files).length > 0) {
      if (req.files.desktop_img != undefined) {
        let desktop_img = req.files.desktop_img;
        desktop_img.name = desktop_img.name.replace(/ /g,"_");
        let filename = Math.floor(Math.random() * 100000) + '-' + desktop_img.name;
        let file_url = './upload/banner/' + filename;
        let file_name = 'upload/banner/' + filename;
        desktop_img.mv(file_url, function (error) {
          if (error) {
            res.json({status: 400, message: error});
          } else {
            data.desktop_img = file_name;
          }
        });
      }
      if (req.files.mobile_img != undefined) {
        let mobile_img = req.files.mobile_img;
        mobile_img.name = mobile_img.name.replace(/ /g,"_");
        let filename1 = Math.floor(Math.random() * 100000) + '-' + mobile_img.name;
        let file_url1 = './upload/banner/' + filename1;
        let file_name1 = 'upload/banner/' + filename1;
        mobile_img.mv(file_url1, function (error) {
          if (error) {
            res.json({status: 400, message: error});
          } else {
            data.mobile_img = file_name1;
          }
        });
      }
    }

    let sql = "SELECT * FROM `home_silders` WHERE banner_id = ?";
    conn.query(sql, [banner_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          let sql = "UPDATE `home_silders` SET ? WHERE banner_id = ?";
          conn.query(sql, [data, banner_id], (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Slider update successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }else{
          let sql = "INSERT INTO `home_silders` SET ?";
          conn.query(sql, data, (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Slider created successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }
      }
    });
  }
});

router.post('/addGallery',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    let {gallery_id,position} = req.body;

    let data = {
      position : position,
      created_at : new Date()
    };

    if (req.files && Object.keys(req.files).length > 0) {
      if (req.files.gallery != undefined) {
        let gallery = req.files.gallery;
        gallery.name = gallery.name.replace(/ /g,"_");
        let filename = Math.floor(Math.random() * 100000) + '-' + gallery.name;
        let file_url = './upload/gallery/'+filename;
        let file_name = 'upload/gallery/'+filename;
        gallery.mv(file_url, function (error) {
          if (error) {
            res.json({status: 400, message: error});
          } else {
            data.image = file_name;
          }
        });
      }
    }
    let sql = "SELECT * FROM `gallery` WHERE gallery_id = ?";
    conn.query(sql, [gallery_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          let sql = "UPDATE `gallery` SET ? WHERE gallery_id = ?";
          conn.query(sql, [data, gallery_id], (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Gallery Image update successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }else{
          let sql = "INSERT INTO `gallery` SET ?";
          conn.query(sql, data, (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Gallery Image created successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }
      }
    });
  }
});

router.post('/GetSliders',[
  check('columns').exists(),
  check('start').exists(),
  check('order').exists(),
  check('length').exists()
],(req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.status(201).json({
      success: false,
      message: err
    });
    return;
  } else {
    var { columns, order, start, length } = req.body;
    let sql = `SELECT * FROM home_silders ORDER BY ${columns[order[0].column].data} ${order[0].dir} LIMIT ${start},${length};SELECT count(*) as cnt FROM home_silders`;
    conn.query(sql, (error, resdata) => {
      if(error){
        res.json({status: 400, message: error});
      }else{
        if(resdata[0].length > 0){
          res.json({status: 200, message: "Get data successfully", response: resdata[0], TotalRecords:resdata[1][0]});
        }else{
          res.json({status: 400, message: 'No data found'});
        }
      }
    });
  }
});

router.post('/GetGallery',[
  check('columns').exists(),
  check('start').exists(),
  check('order').exists(),
  check('length').exists()
],(req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.status(201).json({
      success: false,
      message: err
    });
    return;
  } else {
    var { columns, order, start, length } = req.body;
    let sql = `SELECT * FROM gallery ORDER BY ${columns[order[0].column].data} ${order[0].dir} LIMIT ${start},${length};SELECT count(*) as cnt FROM gallery`;
    conn.query(sql, (error, resdata) => {
      if(error){
        res.json({status: 400, message: error});
      }else{
        if(resdata[0].length > 0){
          res.json({status: 200, message: "Get data successfully", response: resdata[0], TotalRecords:resdata[1][0]});
        }else{
          res.json({status: 400, message: 'No data found'});
        }
      }
    });
  }
});

router.post('/GetRecordSlider',[
  check('banner_id').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    var banner_id = req.body.banner_id;
    let sql = "SELECT * FROM `home_silders` WHERE banner_id = ? ";
    conn.query(sql, [banner_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get data successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});


router.post('/GetRecordGallery',[
  check('gallery_id').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    var gallery_id = req.body.gallery_id;
    let sql = "SELECT * FROM `gallery` WHERE gallery_id = ?";
    conn.query(sql, [gallery_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get data successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/removeSlider',[
  check('banner_id').exists()
], function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    var banner_id = req.body.banner_id;
    let sql = "DELETE FROM `home_silders` WHERE banner_id = ?";
    conn.query(sql, [banner_id], function (err, results) {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        res.json({status: 200, message: "Slider removed successfully"});
      }
    });
  }
});

router.post('/removeGallery',[
  check('gallery_id').exists()
], function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    var gallery_id = req.body.gallery_id;
    let sql = "DELETE FROM `gallery` WHERE gallery_id = ?";
    conn.query(sql, [gallery_id], function (err, results) {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        res.json({status: 200, message: "Gallery Image removed successfully"});
      }
    });
  }
});

router.post('/addCounter',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    let {counter_id,name,total} = req.body;

    let data = {
      name : name,
      total : total
    };

    let sql = "SELECT * FROM `counters` WHERE counter_id = ?";
    conn.query(sql, [counter_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          let sql = "UPDATE `counters` SET ? WHERE counter_id = ?";
          conn.query(sql, [data, counter_id], (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Counter update successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }else{
          let sql = "INSERT INTO `counters` SET ?";
          conn.query(sql, data, (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Counter created successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }
      }
    });
  }
});


router.post('/addVacancy',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    let {vacancy_id,vacancy_title,vacancy_exp} = req.body;

    let data = {
      vacancy_title : vacancy_title,
      vacancy_exp : vacancy_exp
    };

    let sql = "SELECT * FROM `opportunities` WHERE vacancy_id = ?";
    conn.query(sql, [vacancy_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          let sql = "UPDATE `opportunities` SET ? WHERE vacancy_id = ?";
          conn.query(sql, [data, vacancy_id], (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Opportunity update successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }else{
          let sql = "INSERT INTO `opportunities` SET ?";
          conn.query(sql, data, (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Opportunity created successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }
      }
    });
  }
});

router.post('/GetCounters',[
  check('columns').exists(),
  check('start').exists(),
  check('order').exists(),
  check('length').exists()
],(req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.status(201).json({
      success: false,
      message: err
    });
    return;
  } else {
    var { columns, order, start, length } = req.body;
    let sql = `SELECT * FROM counters ORDER BY ${columns[order[0].column].data} ${order[0].dir} LIMIT ${start},${length};SELECT count(*) as cnt FROM counters`;
    conn.query(sql, (error, resdata) => {
      if(error){
        res.json({status: 400, message: error});
      }else{
        if(resdata[0].length > 0){
          res.json({status: 200, message: "Get data successfully", response: resdata[0], TotalRecords:resdata[1][0]});
        }else{
          res.json({status: 400, message: 'No data found'});
        }
      }
    });
  }
});

router.post('/GetVacancy',[
  check('columns').exists(),
  check('start').exists(),
  check('order').exists(),
  check('length').exists()
],(req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.status(201).json({
      success: false,
      message: err
    });
    return;
  } else {
    var { columns, order, start, length } = req.body;
    let sql = `SELECT * FROM opportunities ORDER BY ${columns[order[0].column].data} ${order[0].dir} LIMIT ${start},${length};SELECT count(*) as cnt FROM opportunities`;
    conn.query(sql, (error, resdata) => {
      if(error){
        res.json({status: 400, message: error});
      }else{
        if(resdata[0].length > 0){
          res.json({status: 200, message: "Get data successfully", response: resdata[0], TotalRecords:resdata[1][0]});
        }else{
          res.json({status: 400, message: 'No data found'});
        }
      }
    });
  }
});

router.post('/GetRecordCounter',[
  check('counter_id').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    var counter_id = req.body.counter_id;
    let sql = "SELECT * FROM `counters` WHERE counter_id = ?";
    conn.query(sql, [counter_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get data successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/GetRecordVacancy',[
  check('vacancy_id').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    var vacancy_id = req.body.vacancy_id;
    let sql = "SELECT * FROM `opportunities` WHERE vacancy_id = ?";
    conn.query(sql, [vacancy_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get data successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/removeCounter',[
  check('counter_id').exists()
], function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    var counter_id = req.body.counter_id;
    let sql = "DELETE FROM `counters` WHERE counter_id = ?";
    conn.query(sql, [counter_id], function (err, results) {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        res.json({status: 200, message: "Data removed successfully"});
      }
    });
  }
});

router.post('/removeVacancy',[
  check('vacancy_id').exists()
], function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    var vacancy_id = req.body.vacancy_id;
    let sql = "DELETE FROM `opportunities` WHERE vacancy_id = ?";
    conn.query(sql, [vacancy_id], function (err, results) {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        res.json({status: 200, message: "Data removed successfully"});
      }
    });
  }
});


router.post('/addAbout',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    let {about_id,about_title,position,about_content,about_content2,meta_title,meta_keyword,meta_description} = req.body;

    let slug = about_title
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'')
    ;
    let data = {
      about_title : about_title,
      position : position,
      slug : slug,
      about_content : about_content,
      about_content2 : about_content2,
      meta_title : meta_title,
      meta_keyword : meta_keyword,
      meta_description : meta_description
    };

    if (req.files && Object.keys(req.files).length > 0) {
      if (req.files.image != undefined) {
        let image = req.files.image;
        image.name = image.name.replace(/ /g,"_");
        let filename = Math.floor(Math.random() * 100000) + '-' + image.name;
        let file_url = './upload/about/' + filename;
        let file_name = 'upload/about/' + filename;
        image.mv(file_url, function (error) {
          if (error) {
            res.json({status: 400, message: error});
          } else {
            data.image = file_name;
          }
        });
      }
    }

    let sql = "SELECT * FROM `about` WHERE about_id = ?";
    conn.query(sql, [about_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          let sql = "UPDATE `about` SET ? WHERE about_id = ?";
          conn.query(sql, [data, about_id], (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "About update successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }else{
          let sql = "INSERT INTO `about` SET ?";
          conn.query(sql, data, (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "About created successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }
      }
    });
  }
});

router.post('/GetAbouts',[
  check('columns').exists(),
  check('start').exists(),
  check('order').exists(),
  check('length').exists()
],(req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.status(201).json({
      success: false,
      message: err
    });
    return;
  } else {
    var { columns, order, start, length } = req.body;
    let sql = `SELECT * FROM about ORDER BY ${columns[order[0].column].data} ${order[0].dir} LIMIT ${start},${length};SELECT count(*) as cnt FROM about`;
    conn.query(sql, (error, resdata) => {
      if(error){
        res.json({status: 400, message: error});
      }else{
        if(resdata[0].length > 0){
          res.json({status: 200, message: "Get data successfully", response: resdata[0], TotalRecords:resdata[1][0]});
        }else{
          res.json({status: 400, message: 'No data found'});
        }
      }
    });
  }
});

router.post('/GetRecordAbout',[
  check('about_id').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    var about_id = req.body.about_id;
    let sql = "SELECT * FROM `about` WHERE about_id = ?";
    conn.query(sql, [about_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get data successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/removeAbout',[
  check('about_id').exists()
], function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    var about_id = req.body.about_id;
    let sql = "DELETE FROM `about` WHERE about_id = ?";
    conn.query(sql, [about_id], function (err, results) {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        res.json({status: 200, message: "Data removed successfully"});
      }
    });
  }
});


router.post('/addClient',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    let {client_id} = req.body;

    let data = {
      created_at : new Date()
    };

    if (req.files && Object.keys(req.files).length > 0) {
      if (req.files.image != undefined) {
        let image = req.files.image;
        image.name = image.name.replace(/ /g,"_");
        let filename = Math.floor(Math.random() * 100000) + '-' + image.name;
        let file_url = './upload/client/' + filename;
        let file_name = 'upload/client/' + filename;
        image.mv(file_url, function (error) {
          if (error) {
            res.json({status: 400, message: error});
          } else {
            data.image = file_name;
          }
        });
      }
    }

    let sql = "SELECT * FROM `clients` WHERE client_id = ?";
    conn.query(sql, [client_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          let sql = "UPDATE `clients` SET ? WHERE client_id = ?";
          conn.query(sql, [data, client_id], (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Client update successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }else{
          let sql = "INSERT INTO `clients` SET ?";
          conn.query(sql, data, (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Client created successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }
      }
    });
  }
});

router.post('/GetClients',[
  check('columns').exists(),
  check('start').exists(),
  check('order').exists(),
  check('length').exists()
],(req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.status(201).json({
      success: false,
      message: err
    });
    return;
  } else {
    var { columns, order, start, length } = req.body;
    let sql = `SELECT * FROM clients ORDER BY ${columns[order[0].column].data} ${order[0].dir} LIMIT ${start},${length};SELECT count(*) as cnt FROM clients`;
    conn.query(sql, (error, resdata) => {
      if(error){
        res.json({status: 400, message: error});
      }else{
        if(resdata[0].length > 0){
          res.json({status: 200, message: "Get data successfully", response: resdata[0], TotalRecords:resdata[1][0]});
        }else{
          res.json({status: 400, message: 'No data found'});
        }
      }
    });
  }
});

router.post('/GetRecordClient',[
  check('client_id').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    var client_id = req.body.client_id;
    let sql = "SELECT * FROM `clients` WHERE client_id = ?";
    conn.query(sql, [client_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get data successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/removeClient',[
  check('client_id').exists()
], function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    var client_id = req.body.client_id;
    let sql = "DELETE FROM `clients` WHERE client_id = ?";
    conn.query(sql, [client_id], function (err, results) {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        res.json({status: 200, message: "Data removed successfully"});
      }
    });
  }
});


router.post('/addTestimonial',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    let {testimonial_id,name,description,position} = req.body;
    let data = {
      name:name,
      position:position,
      description:description,
    }
    if (req.files && Object.keys(req.files).length > 0) {
      if (req.files.image != undefined) {
        let image = req.files.image;
        image.name = image.name.replace(/ /g,"_");
        let filename = Math.floor(Math.random() * 100000) + '-' + image.name;
        let file_url = './upload/testimonial/' + filename;
        let file_name = 'upload/testimonial/' + filename;
        image.mv(file_url, function (error) {
          if (error) {
            res.json({status: 400, message: error});
          } else {
            data.image = file_name;
          }
        });
      }
    }
    let sql = "SELECT * FROM `testimonials` WHERE testimonial_id = ?";
    conn.query(sql, [testimonial_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          let sql = "UPDATE `testimonials` SET ? WHERE testimonial_id = ?";
          conn.query(sql, [data, testimonial_id], (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Testimonial update successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }else{
          let sql = "INSERT INTO `testimonials` SET ?";
          conn.query(sql, data, (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Testimonial created successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }
      }
    });
  }
});

router.post('/GetTestimonials',[
  check('columns').exists(),
  check('start').exists(),
  check('order').exists(),
  check('length').exists()
],(req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.status(201).json({
      success: false,
      message: err
    });
    return;
  } else {
    var { columns, order, start, length } = req.body;
    let sql = `SELECT * FROM testimonials ORDER BY ${columns[order[0].column].data} ${order[0].dir} LIMIT ${start},${length};SELECT count(*) as cnt FROM testimonials`;
    conn.query(sql, (error, resdata) => {
      if(error){
        res.json({status: 400, message: error});
      }else{
        if(resdata[0].length > 0){
          res.json({status: 200, message: "Get data successfully", response: resdata[0], TotalRecords:resdata[1][0]});
        }else{
          res.json({status: 400, message: 'No data found'});
        }
      }
    });
  }
});

router.post('/GetRecordTestimonial',[
  check('testimonial_id').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    var testimonial_id = req.body.testimonial_id;
    let sql = "SELECT * FROM `testimonials` WHERE testimonial_id = ?";
    conn.query(sql, [testimonial_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get data successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/removeTestimonial',[
  check('testimonial_id').exists()
], function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    var testimonial_id = req.body.testimonial_id;
    let sql = "DELETE FROM `testimonials` WHERE testimonial_id = ?";
    conn.query(sql, [testimonial_id], function (err, results) {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        res.json({status: 200, message: "Data removed successfully"});
      }
    });
  }
});

router.post('/GetCareers',[
  check('columns').exists(),
  check('start').exists(),
  check('order').exists(),
  check('length').exists()
],(req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.status(201).json({
      success: false,
      message: err
    });
    return;
  } else {
    var { columns, order, start, length } = req.body;
    let sql = `SELECT * FROM careers ORDER BY ${columns[order[0].column].data} ${order[0].dir} LIMIT ${start},${length};SELECT count(*) as cnt FROM careers`;
    conn.query(sql, (error, resdata) => {
      if(error){
        res.json({status: 400, message: error});
      }else{
        if(resdata[0].length > 0){
          res.json({status: 200, message: "Get data successfully", response: resdata[0], TotalRecords:resdata[1][0]});
        }else{
          res.json({status: 400, message: 'No data found'});
        }
      }
    });
  }
});

router.post('/GetVendor',[
  check('columns').exists(),
  check('start').exists(),
  check('order').exists(),
  check('length').exists()
],(req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.status(201).json({
      success: false,
      message: err
    });
    return;
  } else {
    var { columns, order, start, length } = req.body;
    let sql = `SELECT * FROM vendor ORDER BY ${columns[order[0].column].data} ${order[0].dir} LIMIT ${start},${length};SELECT count(*) as cnt FROM vendor`;
    conn.query(sql, (error, resdata) => {
      if(error){
        res.json({status: 400, message: error});
      }else{
        if(resdata[0].length > 0){
          res.json({status: 200, message: "Get data successfully", response: resdata[0], TotalRecords:resdata[1][0]});
        }else{
          res.json({status: 400, message: 'No data found'});
        }
      }
    });
  }
});

router.post('/GetContacts',[
  check('columns').exists(),
  check('start').exists(),
  check('order').exists(),
  check('length').exists()
],(req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.status(201).json({
      success: false,
      message: err
    });
    return;
  } else {
    var { columns, order, start, length } = req.body;
    let sql = `SELECT * FROM contacts ORDER BY ${columns[order[0].column].data} ${order[0].dir} LIMIT ${start},${length};SELECT count(*) as cnt FROM contacts`;
    conn.query(sql, (error, resdata) => {
      if(error){
        res.json({status: 400, message: error});
      }else{
        if(resdata[0].length > 0){
          res.json({status: 200, message: "Get data successfully", response: resdata[0], TotalRecords:resdata[1][0]});
        }else{
          res.json({status: 400, message: 'No data found'});
        }
      }
    });
  }
});

router.post('/removeCareer',[
  check('career_id').exists()
], function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    var career_id = req.body.career_id;
    let sql = "DELETE FROM `careers` WHERE career_id = ?";
    conn.query(sql, [career_id], function (err, results) {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        res.json({status: 200, message: "Data removed successfully"});
      }
    });
  }
});

router.post('/removeVendor',[
  check('vendor_id').exists()
], function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    var vendor_id = req.body.vendor_id;
    let sql = "DELETE FROM `vendor` WHERE vendor_id = ?";
    conn.query(sql, [vendor_id], function (err, results) {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        res.json({status: 200, message: "Data removed successfully"});
      }
    });
  }
});

router.post('/removeContact',[
  check('contact_id').exists()
], function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    var contact_id = req.body.contact_id;
    let sql = "DELETE FROM `contacts` WHERE contact_id = ?";
    conn.query(sql, [contact_id], function (err, results) {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        res.json({status: 200, message: "Data removed successfully"});
      }
    });
  }
});


router.post('/addBrochure',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    let {brochure_id} = req.body;

    let data = {
      created_at : new Date()
    };

    if (req.files && Object.keys(req.files).length > 0) {
      if (req.files.pdf != undefined) {
        let pdf = req.files.pdf;
        pdf.name = pdf.name.replace(/ /g,"_");
        let filename = Math.floor(Math.random() * 100000) + '-' + pdf.name;
        let file_url = './upload/pdf/' + filename;
        let file_name = 'upload/pdf/' + filename;
        pdf.mv(file_url, function (error) {
          if (error) {
            res.json({status: 400, message: error});
          } else {
            data.brochure_file = file_name;
          }
        });
      }
    }

    let sql = "SELECT * FROM `brochure` WHERE brochure_id = ?";
    conn.query(sql, [brochure_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          let sql = "UPDATE `brochure` SET ? WHERE brochure_id = ?";
          conn.query(sql, [data, brochure_id], (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "File upload successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }else{
          let sql = "INSERT INTO `brochure` SET ?";
          conn.query(sql, data, (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "File upload successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }
      }
    });
  }
});

router.post('/addMap',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    let {map_id,map_url} = req.body;

    let data = {
      map_url : map_url
    };

    let sql = "SELECT * FROM `map` WHERE map_id = ?";
    conn.query(sql, [map_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          let sql = "UPDATE `map` SET ? WHERE map_id = ?";
          conn.query(sql, [data, map_id], (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Data upload successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }else{
          let sql = "INSERT INTO `map` SET ?";
          conn.query(sql, data, (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Data upload successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }
      }
    });
  }
});


router.post('/addSeoSettings',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    let {seo_settings_id,google_analytics,home_meta_title,home_meta_keyword,home_meta_description,careers_meta_title,careers_meta_keyword,careers_meta_description,contacts_meta_title,contacts_meta_keyword,contacts_meta_description,clients_meta_title,clients_meta_keyword,clients_meta_description,vendor_meta_title,vendor_meta_keyword,vendor_meta_description} = req.body;

    let data = {
      google_analytics : google_analytics,
      home_meta_title : home_meta_title,
      home_meta_keyword : home_meta_keyword,
      home_meta_description : home_meta_description,
      careers_meta_title : careers_meta_title,
      careers_meta_keyword : careers_meta_keyword,
      careers_meta_description : careers_meta_description,
      contacts_meta_title : contacts_meta_title,
      contacts_meta_keyword : contacts_meta_keyword,
      contacts_meta_description : contacts_meta_description,
      clients_meta_title : clients_meta_title,
      clients_meta_keyword : clients_meta_keyword,
      clients_meta_description : clients_meta_description,
      vendor_meta_title : vendor_meta_title,
      vendor_meta_keyword : vendor_meta_keyword,
      vendor_meta_description : vendor_meta_description,
    };

    let sql = "SELECT * FROM `seo_settings` WHERE seo_settings_id = ?";
    conn.query(sql, [seo_settings_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          let sql = "UPDATE `seo_settings` SET ? WHERE seo_settings_id = ?";
          conn.query(sql, [data, seo_settings_id], (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Data update successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }else{
          let sql = "INSERT INTO `seo_settings` SET ?";
          conn.query(sql, data, (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Data added successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }
      }
    });
  }
});

router.post('/addAddress',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    let {address_id,office_address} = req.body;

    let data = {
      office_address : office_address
    };

    let sql = "SELECT * FROM `address` WHERE address_id = ?";
    conn.query(sql, [address_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          let sql = "UPDATE `address` SET ? WHERE address_id = ?";
          conn.query(sql, [data, address_id], (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Data update successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }else{
          let sql = "INSERT INTO `address` SET ?";
          conn.query(sql, data, (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Data added successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }
      }
    });
  }
});

router.post('/GetRecordBrochure',[
  check('brochure_id').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    var brochure_id = req.body.brochure_id;
    let sql = "SELECT * FROM `brochure` WHERE brochure_id = ?";
    conn.query(sql, [brochure_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          let brochure_file_name = results[0].brochure_file.split('/').pop()
          results[0].brochure_file_name = brochure_file_name
          res.json({status: 200, message: 'Get data successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/GetRecordSeoSettings',[
  check('seo_settings_id').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    var seo_settings_id = req.body.seo_settings_id;
    let sql = "SELECT * FROM `seo_settings` WHERE seo_settings_id = ?";
    conn.query(sql, [seo_settings_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get data successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/GetRecordAddress',[
  check('address_id').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    var address_id = req.body.address_id;
    let sql = "SELECT * FROM `address` WHERE address_id = ?";
    conn.query(sql, [address_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get data successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});


router.post('/GenerateSitemap', function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    // create generator
    const generator = SitemapGenerator(FRONTEND_URL, {
      maxDepth: 0,
      filepath: './sitemap.xml',
      maxEntriesPerFile: 50000,
      stripQuerystring: true
    });

    // register event listeners
    generator.on('done', () => {
      // sitemaps created
    });

    // start the crawler
    generator.start();
  }
});


router.post('/GetRecordMap',[
  check('map_id').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    var map_id = req.body.map_id;
    let sql = "SELECT * FROM `map` WHERE map_id = ?";
    conn.query(sql, [map_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get data successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/addMedia',function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    let {media_id,media_name,media_href} = req.body;
    let data = {
      media_name : media_name,
      media_href : media_href
    };
    let sql = "SELECT * FROM `media` WHERE media_id = ?";
    conn.query(sql, [media_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          let sql = "UPDATE `media` SET ? WHERE media_id = ?";
          conn.query(sql, [data, media_id], (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Data update successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }else{
          let sql = "INSERT INTO `media` SET ?";
          conn.query(sql, data, (err, results) => {
            if (err) {
              res.json({status: 400, message: err});
            } else {
              if (results.affectedRows === 1) {
                res.json({status: 200, message: "Data created successfully", response: results[0]});
              } else {
                res.json({status: 400, message: 'No data Found'});
              }
            }
          });
        }
      }
    });
  }
});

router.post('/GetRecordMedia',[
  check('media_id').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    var media_id = req.body.media_id;
    let sql = "SELECT * FROM `media` WHERE media_id = ?";
    conn.query(sql, [media_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get data successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/GetMedia',[
  check('columns').exists(),
  check('start').exists(),
  check('order').exists(),
  check('length').exists()
],(req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.status(201).json({
      success: false,
      message: err
    });
    return;
  } else {
    var { columns, order, start, length } = req.body;
    let sql = `SELECT * FROM media ORDER BY ${columns[order[0].column].data} ${order[0].dir} LIMIT ${start},${length};SELECT count(*) as cnt FROM media`;
    conn.query(sql, (error, resdata) => {
      if(error){
        res.json({status: 400, message: error});
      }else{
        if(resdata[0].length > 0){
          res.json({status: 200, message: "Get data successfully", response: resdata[0], TotalRecords:resdata[1][0]});
        }else{
          res.json({status: 400, message: 'No data found'});
        }
      }
    });
  }
});

router.post('/removeMedia',[
  check('media_id').exists()
], function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({status: 422, message: errors.array()});
  }else {
    var media_id = req.body.media_id;
    let sql = "DELETE FROM `media` WHERE media_id = ?";
    conn.query(sql, [media_id], function (err, results) {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        res.json({status: 200, message: "Data removed successfully"});
      }
    });
  }
});


module.exports = router;
